<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\AdministradorController;
use App\Http\Controllers\RecepcionesController;
use App\Http\Controllers\CcpController;
use App\Http\Controllers\TurnosController;
/*
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('login',[LoginController::class, 'login'])->name('login');
ROUTE::POST('valida',[LoginController::class,'valida'])->name('valida');
ROUTE::GET('principal',[LoginController::class, 'principal'])->name('principal');
ROUTE::GET('cerrar_sesion', [LoginController::class, 'cerrar_sesion'])->name('cerrar_sesion');
//ADMINISTRADOR
ROUTE::GET('alta_usuarios',[AdministradorController::class, 'alta_usuarios'])->name('alta_usuarios');
ROUTE::POST('guarda_usuarios',[AdministradorController::class, 'guarda_usuarios'])->name('guarda_usuarios');
ROUTE::GET('busqueda_usuarios',[AdministradorController::class, 'busqueda_usuarios'])->name('busqueda_usuarios');
ROUTE::GET('usuarios_lista_json',[AdministradorController::class, 'usuarios_lista_json'])->name('usuarios_lista_json');
ROUTE::GET('modifica_usuarios/{id_usuario}',[AdministradorController::class, 'modifica_usuarios'])->name('modifica_usuarios');
ROUTE::POST('actualiza_usuario',[AdministradorController::class, 'actualiza_usuario'])->name('actualiza_usuario');
ROUTE::GET('consulta_direcciones',[AdministradorController::class, 'consulta_direcciones'])->name('consulta_direcciones');
ROUTE::POST('alta_direcciones', [AdministradorController::class, 'alta_direcciones'])->name('alta_direcciones');
ROUTE::GET('direcciones_lista_json', [AdministradorController::class, 'direcciones_lista_json'])->name('direcciones_lista_json');
ROUTE::GET('modifica_direccion/{id_direccion}', [AdministradorController::class, 'modifica_direccion'])->name('modifica_direccion');
ROUTE::POST('actualiza_direccion', [AdministradorController::class, 'actualiza_direccion'])->name('actualiza_direccion');
ROUTE::GET('consulta_directivos', [AdministradorController::class, 'consulta_directivos'])->name('consulta_directivos');
ROUTE::GET('directivos_lista_json', [AdministradorController::class, 'directivos_lista_json'])->name('directivos_lista_json');
ROUTE::GET('modifica_directivos/{id_directivo}',[AdministradorController::class, 'modifica_directivos'])->name('modifica_directivos');
ROUTE::POST('actualiza_directivos', [AdministradorController::class, 'actualiza_directivos'])->name('actualiza_directivos');
ROUTE::POST('alta_directivos', [AdministradorController::class, 'alta_directivos'])->name('alta_directivos');
ROUTE::GET('consulta_remitentes',[AdministradorController::class, 'consulta_remitentes'])->name('consulta_remitentes');
ROUTE::GET('remitentes_lista_json', [AdministradorController::class, 'remitentes_lista_json'])->name('remitentes_lista_json');
ROUTE::GET('modifica_remitentes/{id}',[AdministradorController::class, 'modifica_remitentes'])->name('modifica_remitentes');
ROUTE::POST('actualiza_remitente',[AdministradorController::class, 'actualiza_remitente'])->name('actualiza_remitente');
ROUTE::POST('alta_remitente',[AdministradorController::class, 'alta_remitente'])->name('alta_remitente');
ROUTE::GET('consulta_documentos', [AdministradorController::class, 'consulta_documentos'])->name('consulta_documentos');
ROUTE::GET('modifica_documentos/{id}',[AdministradorController::class, 'modifica_documentos'])->name('modifica_documentos');
ROUTE::GET('documentos_lista_json', [AdministradorController::class, 'documentos_lista_json'])->name('documentos_lista_json');
ROUTE::POST('actualiza_documento',[AdministradorController::class, 'actualiza_documento'])->name('actualiza_documento');
ROUTE::POST('alta_documentos',[AdministradorController::class, 'alta_documentos'])->name('alta_documentos');
//RECEPCIONES
ROUTE::GET('recepciones',[RecepcionesController::class, 'recepciones'])->name('recepciones');
ROUTE::POST('guarda_recepcion',[RecepcionesController::class, 'guarda_recepciones'])->name('guarda_recepciones');
ROUTE::GET('recepcion_lista_json',[RecepcionesController::class, 'recepcion_lista_json'])->name('recepcion_lista_json');
ROUTE::POST('get_expediente',[RecepcionesController::class, 'get_expediente'])->name('get_expediente');
//BÚSQUEDAS
ROUTE::GET('busqueda_recepcion',[RecepcionesController::class, 'busqueda_recepcion'])->name('busqueda_recepcion');
ROUTE::GET('consulta_recepciones',[RecepcionesController::class, 'consulta_recepciones'])->name('consulta_recepciones');
ROUTE::GET('busqueda_general',[RecepcionesController::class, 'busqueda_general'])->name('busqueda_general');
ROUTE::GET('general_lista_json',[RecepcionesController::class, 'general_lista_json'])->name('general_lista_json');
ROUTE::GET('muestra_general/{id}',[RecepcionesController::class, 'muestra_general'])->name('muestra_general');
//MODIFICACIONES
ROUTE::GET('modifica_recepcion/{id}',[RecepcionesController::class,'modifica_recepciones'])->name('modifica_recepcion');
ROUTE::POST('guarda_actualizacion', [RecepcionesController::class, 'guarda_actualizacion'])->name('guarda_actualizacion');
//CPP funciones
ROUTE::GET('busqueda_ccp',[CcpController::class, 'busqueda_ccp'])->name('busqueda_ccp');
ROUTE::GET('ccp_lista_json',[CcpController::class, 'ccp_lista_json'])->name('ccp_lista_json');
//Turnos funciones
ROUTE::GET('busqueda_turnos',[TurnosController::class, 'busqueda_turnos'])->name('busqueda_turnos');
ROUTE::GET('turnos_lista_json',[TurnosController::class, 'turnos_lista_json'])->name('turnos_lista_json');
ROUTE::GET('turnar_recepcion/{id}', [TurnosController::class, 'turnar_recepcion'])->name('turnar_recepcion');
ROUTE::POST('turnar',[TurnosController::class, 'turnar'])->name('turnar');
ROUTE::GET('descarga_respuesta/{archivo}', [TurnosController::class, 'descarga_respuesta'])->name('descarga_respuesta');
ROUTE::GET('eliminar_respuesta/{id}', [TurnosController::class, 'eliminar_respuesta'])->name('eliminar_respuesta');
//RESPUESTAS TURNO
ROUTE::GET('respuesta_turnado/{id}',[TurnosController::class, 'respuesta_turnado'])->name('respuesta_turnado');
ROUTE::POST('guarda_respuesta_turnado',[TurnosController::class, 'guarda_respuesta_turnado'])->name('guarda_respuesta_turnado');


ROUTE::GET('download/{archivo}', [RecepcionesController::class, 'descargas'])->name('download');
ROUTE::GET('download_pdf/{id}', [RecepcionesController::class, 'download_pdf'])->name('download_pdf');
ROUTE::GET('eliminar_archivo/{archivo}', [RecepcionesController::class, 'eliminar_archivo'])->name('eliminar_archivo');





$("#nomenclatura").change(function() {

    var id_nomenclatura = $("#nomenclatura").val();
    var uRL = window.location.hostname;
    //console.log(id_nomenclatura);
    console.log(uRL);

    if (uRL == '127.0.0.1') {
        uRL = 'http://127.0.0.1:8000';
    }
    if (uRL == 'fiscalia.edomex.gob.mx') {

        uRL = 'http://fiscalia.edomex.gob.mx/sis/gestion';
    }



    url = uRL + "/get_expediente/";
    console.log(url);

    $.ajax({
        url: url,
        type: 'post',
        data: {
            "id_nomenclatura": id_nomenclatura,
            "_token": $("meta[name='csrf-token']").attr("content")
        },
        success: function(data) {
            $('#expediente').attr('readonly', true);
            $("#expediente").val(data);
        }
    });
});
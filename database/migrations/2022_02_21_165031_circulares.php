<?php


use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Circulares extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('circulares', function (Blueprint $table){
            $table->id('id_circular');
            $table->date('c_fecha');
            $table->string('c_destinatario');
            $table->string('c_cargo');
            $table->string('c_asunto');
            $table->string('c_solicitante');
            $table->string('archivo');
            $table->unsignedBigInteger('id_direccion');
            $table->foreign('id_direccion')->references('id_direccion')->on('direcciones');
            $table->boolean('activo');
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::drop('circulares');
    }
}

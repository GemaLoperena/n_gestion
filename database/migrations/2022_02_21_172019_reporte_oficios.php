<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReporteOficios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reporte_oficios', function (Blueprint $table){
            $table->id('id_reporte_oficio');
            $table->date('ro_fecha');
            $table->time('ro_hora');
            $table->longtext('ro_descripcion');
            $table->string('ro_archivo');
            $table->unsignedBigInteger('id_oficio');
            $table->foreign('id_oficio')->references('id_oficio')->on('oficios');
            $table->unsignedBigInteger('id_usuario');
            $table->foreign('id_usuario')->references('id_usuario')->on('usuarios');
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::drop('reporte_oficios');
    }
}

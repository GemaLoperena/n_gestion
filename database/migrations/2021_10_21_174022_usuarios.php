<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Usuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->id('id_usuario');
            $table->string('nom_usuario');
            $table->string('app_usuario')->nullable();
            $table->string('apm_usuario')->nullable();
            $table->string('usuario');
            $table->string('contrasena');
            $table->unsignedBigInteger('id_rol');
            $table->foreign('id_rol')->references('id_rol')->on('roles');
            $table->unsignedBigInteger('id_direccion');
            $table->foreign('id_direccion')->references('id_direccion')->on('direcciones');
            $table->string('profesion')->nullable();
            $table->boolean('activo');
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::drop('usuarios');
    }
}

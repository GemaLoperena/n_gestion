<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Recepciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recepciones', function(Blueprint $table){
            $table->id('id_recepcion');
            $table->unsignedBigInteger('id_nomenclatura');
            $table->foreign('id_nomenclatura')->references('id_nomenclatura')->on('nomenclaturas');
            $table->integer('no_expediente');
            $table->integer('turno');
            $table->string('no_oficio')->nullable();
            $table->date('fecha_oficio');
            $table->date('fecha_recepcion');
            $table->time('hora_recepcion');
            $table->longtext('asunto');
            $table->string('anio');
            $table->unsignedBigInteger('id_usuario');
            $table->foreign('id_usuario')->references('id_usuario')->on('usuarios');
            $table->unsignedBigInteger('id_status');
            $table->foreign('id_status')->references('id_status')->on('status');
            $table->string('situacion');
            $table->unsignedBigInteger('id_criterio');
            $table->foreign('id_criterio')->references('id_criterio')->on('criterios');
            $table->string('vigencia')->nullable();
            $table->boolean('procedencia')->nullable();
            $table->string('observaciones')->nullable();
            $table->string('prioritario')->nullable();
            $table->unsignedBigInteger('id_tdocumento');
            $table->foreign('id_tdocumento')->references('id_tdocumento')->on('tipodocumentos');
            $table->unsignedBigInteger('id_remitente');
            $table->foreign('id_remitente')->references('id_remitente')->on('remitentes');
            $table->string('r_conjunta');
            $table->string('asignado');
            $table->string('archivo')->nullable();
            $table->string('activo');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('recepciones');
    }
}


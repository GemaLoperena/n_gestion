<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Turnados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('turnados', function(Blueprint $table){
            $table->id('id_turnado');
            $table->longText('turnado')->nullable();
            $table->longText('ccp')->nullable();
            $table->unsignedBigInteger('id_recepcion');
            $table->foreign('id_recepcion')->references('id_recepcion')->on('recepciones');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('turnados');

    }
}

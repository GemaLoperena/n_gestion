<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Roles extends Migration
{
    public function up()
    {
        Schema::create('roles', function (Blueprint $table){
            $table->id('id_rol');
            $table->string('rol');
        });

    }
    public function down()
    {
        Schema::drop('roles');
    }
}

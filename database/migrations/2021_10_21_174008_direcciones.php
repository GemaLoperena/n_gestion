<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Direcciones extends Migration
{
    public function up()
    {
        Schema::create('direcciones', function (Blueprint $table){
            $table->id('id_direccion');
            $table->string('direccion');
            $table->string('acronimo');
            $table->boolean('activo');
            $table->timestamps();
        });
    }
   public function down()
    {
        Schema::drop('direcciones');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReporteCirculares extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reporte_circulares', function (Blueprint $table){
            $table->id('id_reporte_circular');
            $table->date('rc_fecha');
            $table->time('rc_hora');
            $table->longtext('rc_descripcion');
            $table->string('rc_archivo');
            $table->unsignedBigInteger('id_circular');
            $table->foreign('id_circular')->references('id_circular')->on('circulares');
            $table->unsignedBigInteger('id_usuario');
            $table->foreign('id_usuario')->references('id_usuario')->on('usuarios');
            $table->timestamps();
        });
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reporte_circulares');
    }
}

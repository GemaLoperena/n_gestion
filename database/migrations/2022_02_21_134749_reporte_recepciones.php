<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReporteRecepciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reporte_recepciones', function(Blueprint $table){
            $table->id('id_reporte_recepciones');
            $table->date('rr_fecha')->nullable();
            $table->time('rr_hora');
            $table->longtext('rr_descripcion');
            $table->string('rr_archivo')->nullable();
            $table->unsignedBigInteger('id_recepcion');
            $table->foreign('id_recepcion')->references('id_recepcion')->on('recepciones');
            $table->unsignedBigInteger('id_usuario');
            $table->foreign('id_usuario')->references('id_usuario')->on('usuarios');
            $table->unsignedBigInteger('tipo_reporte');
            $table->boolean('activo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reporte_recepciones');
    }
}

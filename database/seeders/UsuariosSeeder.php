<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usuarios')->insert(['nom_usuario' => 'ADMINISTRADOR','app_usuario' => ' ','apm_usuario' => ' ','id_rol' => '1','id_direccion'=>1,'usuario' => 'administrador','contrasena' => Hash::make('12345678'),'activo'=> 1, 'profesion'=>'ING']);
        DB::table('usuarios')->insert(['nom_usuario' => 'SEGUIMIENTO','app_usuario' => ' ','apm_usuario' => ' ','id_rol' => '4','id_direccion'=>4,'usuario' => 'seguimiento','contrasena' => Hash::make('12345678'),'activo'=> 1, 'profesion'=>'ING']);
        DB::table('usuarios')->insert(['nom_usuario' => 'CONTROL','app_usuario' => ' ','apm_usuario' => ' ','id_rol' => '5','id_direccion'=>5,'usuario' => 'control','contrasena' => Hash::make('12345678'),'activo'=> 1, 'profesion'=>'ING']);
        DB::table('usuarios')->insert(['nom_usuario' => 'PRUEBA','app_usuario' => 'DE','apm_usuario' => 'ADMINISTRADOR','id_rol' => '1','id_direccion'=>6,'usuario' => 'prueba.admin','contrasena' => Hash::make('12345678'),'activo'=> 1, 'profesion'=>'ING']);
        DB::table('usuarios')->insert(['nom_usuario' => 'PRUEBA','app_usuario' => 'DE','apm_usuario' => 'SEGUIMIENTO','id_rol' => '4','id_direccion'=>2,'usuario' => 'prueba.seguimiento','contrasena' => Hash::make('12345678'),'activo'=> 1, 'profesion'=>'ING']);
        DB::table('usuarios')->insert(['nom_usuario' => 'PRUEBA','app_usuario' => 'DE','apm_usuario' => 'CONTROL','id_rol' => '5','id_direccion'=>3,'usuario' => 'prueba.control','contrasena' => Hash::make('12345678'),'activo'=> 1, 'profesion'=>'ING']);
        

    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NomenclaturasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('nomenclaturas')->insert([
            'id_nomenclatura' => 1,
            'nomenclatura' => 'AC',
            'descripcion' => 'ACUERDO',
            'activo' => 1
        ]);

        DB::table('nomenclaturas')->insert([
            'id_nomenclatura' => 2,
            'nomenclatura' => 'CO',
            'descripcion' => 'COLABORACIÓN',
            'activo' => 1
        ]);

        DB::table('nomenclaturas')->insert([
            'id_nomenclatura' => 3,
            'nomenclatura' => 'ED',
            'descripcion' => 'DENUNCIA',
            'activo' => 1
        ]);

        DB::table('nomenclaturas')->insert([
            'id_nomenclatura' => 4,
            'nomenclatura' => 'DV',
            'descripcion' => 'DEVOLUCIÓN',
            'activo' => 1
        ]);

        DB::table('nomenclaturas')->insert([
            'id_nomenclatura' => 5,
            'nomenclatura' => 'ES',
            'descripcion' => 'ESCRITO',
            'activo' => 1
        ]);

        DB::table('nomenclaturas')->insert([
            'id_nomenclatura' => 6,
            'nomenclatura' => 'IN',
            'descripcion' => 'INCOMPETENCIA',
            'activo' => 1
        ]);

        DB::table('nomenclaturas')->insert([
            'id_nomenclatura' => 7,
            'nomenclatura' => 'MJ',
            'descripcion' => 'MANDAMIENTO JUDICIAL',
            'activo' => 1
        ]);

        DB::table('nomenclaturas')->insert([
            'id_nomenclatura' => 8,
            'nomenclatura' => 'TE',
            'descripcion' => 'TELEGRAMA',
            'activo' => 1
        ]);

        DB::table('nomenclaturas')->insert([
            'id_nomenclatura' => 9,
            'nomenclatura' => 'CE',
            'descripcion' => 'CORREO ELECTRÓNICO',
            'activo' => 1
        ]);

        DB::table('nomenclaturas')->insert([
            'id_nomenclatura' => 10,
            'nomenclatura' => 'OT',
            'descripcion' => 'OTROS',
            'activo' => 1
        ]);
    }
}

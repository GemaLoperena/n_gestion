<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class CriteriosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('criterios')->insert(['criterio' => 'BAJO']);
        DB::table('criterios')->insert(['criterio' => 'MEDIO']);
        DB::table('criterios')->insert(['criterio' => 'CRITICO']);
    }
}

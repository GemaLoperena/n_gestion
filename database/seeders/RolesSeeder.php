<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesSeeder extends Seeder
{
    public function run()
    {
        DB::table('roles')->insert(['rol' => 'ADMINISTRADOR']);
        DB::table('roles')->insert(['rol' => 'CONSULTA GENERAL']);
        DB::table('roles')->insert(['rol' => 'RECEPCION']);
        DB::table('roles')->insert(['rol' => 'SEGUIMIENTO']);
        DB::table('roles')->insert(['rol' => 'CONTROL']);
        DB::table('roles')->insert(['rol' => 'TITULARES']);
    }
}

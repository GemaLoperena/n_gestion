<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class DirectivosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('directivos')->insert(['nombre' => 'QUIMICA MÓNICA MANZANO HERNANDEZ', 'id_direccion' => '1','activo' => '1']);
        DB::table('directivos')->insert(['nombre' => 'MTRO. ÁNGEL IVÁN LUGO COLÍN', 'id_direccion' => '2','activo' => '1']);
        DB::table('directivos')->insert(['nombre' => 'LIC. GERARDO JIMÉNEZ GONZÁLEZ', 'id_direccion' => '10','activo' => '1']);
        DB::table('directivos')->insert(['nombre' => 'LIC. OSCAR CALIXTO SÁNCHEZ', 'id_direccion' => '3','activo' => '1']);
        DB::table('directivos')->insert(['nombre' => 'LIC. INSONNY EUGENIA KUANTAY PÉREZ', 'id_direccion' => '4','activo' => '1']);
        DB::table('directivos')->insert(['nombre' => 'LIC. JOSÉ MANUEL SALAZAR AYALA', 'id_direccion' => '6','activo' => '1']);
        DB::table('directivos')->insert(['nombre' => 'LIC. ALEJANDRO BLANCO CARDENAS', 'id_direccion' => '7','activo' => '1']);
        DB::table('directivos')->insert(['nombre' => 'ING. JUAN ROBERTO RAMIREZ SOTO', 'id_direccion' => '11','activo' => '1']);
        DB::table('directivos')->insert(['nombre' => 'LIC. CARLOS TRUJILLO PEDRAZA', 'id_direccion' => '9','activo' => '1']);
        DB::table('directivos')->insert(['nombre' => 'LIC. GEOVANNA MERARI CALISTO ROJAS', 'id_direccion' => '5','activo' => '1']);
        DB::table('directivos')->insert(['nombre' => 'LIC. YAMILIT LEYVA GUTIÉRREZ', 'id_direccion' => '8','activo' => '1']);
        DB::table('directivos')->insert(['nombre' => 'ING. PAOLA SCHLESKE POSADAS', 'id_direccion' => '12','activo' => '1']);
        DB::table('directivos')->insert(['nombre' => 'DR. RUBÉN DURÁN MIRANDA', 'id_direccion' => '13','activo' => '1']);
        DB::table('directivos')->insert(['nombre' => 'L.C.P RAFAEL RODRIGO BENET ROSILLO', 'id_direccion' => '14','activo' => '1']);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoDocumentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipodocumentos')->insert([
            'id_tdocumento' => 1,
            'descripcion' => 'CIRCULAR',
            'activo' => 1
        ]);

        DB::table('tipodocumentos')->insert([
            'id_tdocumento' => 2,
            'descripcion' => 'COPIA DE CONOCIMIENTO',
            'activo' => 1
        ]);

        DB::table('tipodocumentos')->insert([
            'id_tdocumento' => 3,
            'descripcion' => 'ESCRITO',
            'activo' => 1
        ]);

        DB::table('tipodocumentos')->insert([
            'id_tdocumento' => 4,
            'descripcion' => 'FELICITACIÓN',
            'activo' => 1
        ]);

        DB::table('tipodocumentos')->insert([
            'id_tdocumento' => 5,
            'descripcion' => 'INFORME',
            'activo' => 1
        ]);

        DB::table('tipodocumentos')->insert([
            'id_tdocumento' => 6,
            'descripcion' => 'INVITACIÓN',
            'activo' => 1
        ]);

        DB::table('tipodocumentos')->insert([
            'id_tdocumento' => 7,
            'descripcion' => 'MEMORANDUM',
            'activo' => 1
        ]);

        DB::table('tipodocumentos')->insert([
            'id_tdocumento' => 8,
            'descripcion' => 'OFICIO',
            'activo' => 1
        ]);

        DB::table('tipodocumentos')->insert([
            'id_tdocumento' => 9,
            'descripcion' => 'TARJETA INFORMATIVA',
            'activo' => 1
        ]);

        DB::table('tipodocumentos')->insert([
            'id_tdocumento' => 10,
            'descripcion' => 'OTROS',
            'activo' => 1
        ]);
    }
}

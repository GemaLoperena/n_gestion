<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class InstruccionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('instrucciones')->insert(['instruccion' => 'ACG / GESTIONAR PETICIÓN Y REMITIR CONSTANCIAS QUE ACREDITEN LA ATENCIÓN BRINDADA AL PETICIONARIO, COMO LO ES LA HOJA DE REGISTRO DE AUDIENCIA E IDENTIFICACIÓN.','activo' => '1']);
        DB::table('instrucciones')->insert(['instruccion' => 'ASISTIR  A EVENTO EN REPRESENTACIÓN  INSTITUCIONAL Y CONFIRMAR ASISTENCIA.','activo' => '1']);
        DB::table('instrucciones')->insert(['instruccion' => 'CUMPLIMENTAR  Y/O EJECUTAR MANDATO JUDICIAL E INFORMAR A LA AUTORIDAD REQUIRENTE.','activo' => '1']);
        DB::table('instrucciones')->insert(['instruccion' => 'INICIAR CARPETA DE INVESTIGACIÓN Y CITAR AL DENUNCIANTE PARA RATIFICAR  LA MISMA.','activo' => '1']);
        DB::table('instrucciones')->insert(['instruccion' => 'RESOLVER INCONFORMIDAD.','activo' => '1']);
        DB::table('instrucciones')->insert(['instruccion' => 'INTEGRAR Y DETERMINAR LA AVERIGUACIÓN PREVIA Y/O CARPETA DE INVESTIGACIÓN. ','activo' => '1']);
        DB::table('instrucciones')->insert(['instruccion' => 'REALIZAR DILIGENCIAS EN VÍA DE COLABORACIÓN E INFORMAR A LA AUTORIDAD REQUIRENTE.','activo' => '1']);
        DB::table('instrucciones')->insert(['instruccion' => 'REALIZAR ANÁLISIS TÉCNICO-JURÍDICO DE AVERIGUACIÓN PREVIA Y/O CARPETA DE INVESTIGACIÓN.','activo' => '1']);
        DB::table('instrucciones')->insert(['instruccion' => 'REALIZAR GESTIONES  ADMINISTRATIVAS CORRESPONDIENTES.','activo' => '1']);
        DB::table('instrucciones')->insert(['instruccion' => 'ANALIZAR Y ACORDAR LO QUE CONFORME A DERECHO CORRESPONDA.','activo' => '1']);
        DB::table('instrucciones')->insert(['instruccion' => 'RECIBIR PERSONALMENTE AL PETICIONARIO E INFORMAR A ESTA OFICINA.','activo' => '1']);
        DB::table('instrucciones')->insert(['instruccion' => 'PARA SU ATENCIÓN PROCEDENTE.','activo' => '1']);
        DB::table('instrucciones')->insert(['instruccion' => 'OTROS.','activo' => '1']);

        //
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class statusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status')->insert(['status' => 'PENDIENTE']);
        DB::table('status')->insert(['status' => 'CONCLUIDO']);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            DireccionesSeeder::class,
            RolesSeeder::class,
            UsuariosSeeder::class,
            statusSeeder::class,
            DirectivosSeeder::class,
            NomenclaturasSeeder::class,
            RemitentesSeeder::class,
            CriteriosSeeder::class,
            TipoDocumentoSeeder::class,
            InstruccionesSeeder::class
        ]);
    }
}

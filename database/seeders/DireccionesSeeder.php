<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DireccionesSeeder extends Seeder
{
    public function run()
    {
        DB::table('direcciones')->insert(['direccion' => 'DIRECCIÓN GENERAL DE INFORMACIÓN, PLANEACIÓN, PROGRAMACIÓN Y EVALUACIÓN', 'acronimo' =>'DGIPPYE','activo'=>'1']);
        DB::table('direcciones')->insert(['direccion' => 'DIRECCIÓN GENERAL DE ADMINISTRACIÓN', 'acronimo' =>'DGYA','activo'=>'1']);
        DB::table('direcciones')->insert(['direccion' => 'DIRECCIÓN DE PROCEDIMIENTOS ADQUISITIVOS, ALMACÉN E INVENTARIOS', 'acronimo' =>'DDPAAEI','activo'=>'1']);
        DB::table('direcciones')->insert(['direccion' => 'DIRECCIÓN DE ADMINISTRACIÓN DE PERSONAL Y NÓMINA', 'acronimo' =>'DDADPYN','activo'=>'1']);
        DB::table('direcciones')->insert(['direccion' => 'DIRECCIÓN DE RECLUTAMIENTO, SELECCIÓN DE PERSONAL Y DESARROLLO ORGANIZACIONAL', 'acronimo' =>'DDRSDPYDO','activo'=>'1']);
        DB::table('direcciones')->insert(['direccion' => 'DIRECCIÓN DE RECURSOS FINANCIEROS Y CONTROL PRESUPUESTAL', 'acronimo' =>'DDRFYCP','activo'=>'1']);
        DB::table('direcciones')->insert(['direccion' => 'UNIDAD TÉCNICO NORMATIVA', 'acronimo' =>'UTN','activo'=>'1']);
        DB::table('direcciones')->insert(['direccion' => 'UNIDAD DE TRANSPARENCIA Y ACCESO A LA INFORMACIÓN PÚBLICA', 'acronimo' =>'UDTYAALIP','activo'=>'1']);
        DB::table('direcciones')->insert(['direccion' => 'ÁREA DE PROGRAMAS ESPECIALES', 'acronimo' =>'ADPE','activo'=>'1']);
        DB::table('direcciones')->insert(['direccion' => 'DIRECCIÓN DE SERVICIOS GENERALES Y OBRAS', 'acronimo' =>'DDSGYO','activo'=>'1']);
        DB::table('direcciones')->insert(['direccion' => 'DIRECCIÓN GENERAL DE TECNOLOGÍAS DE LA INFORMACIÓN Y COMUNICACIONES', 'acronimo' =>'DGDTDIYC','activo'=>'1']);
        DB::table('direcciones')->insert(['direccion' => 'OFICIALIA MAYOR', 'acronimo' =>'OM','activo'=>'1']);
        DB::table('direcciones')->insert(['direccion' => 'REVISIÓN', 'acronimo' =>'R','activo'=>'1']);
        DB::table('direcciones')->insert(['direccion' => 'DIRECCION DE BODEGA DE EVIDENCIAS', 'acronimo' =>'DDBDE','activo'=>'1']);
    }
}

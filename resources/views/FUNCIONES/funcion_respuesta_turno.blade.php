@extends('OBJETOS.respuesta_turno')
@section('respuestas')
<div class="row">
                    <div class="col-md-4 px-1">
                        <label class="badge bg-primary">Respuesta o comentario</label>
                        <textarea class="form-control textarea" id="rr_descripcion" name="rr_descripcion" onkeyup="Upper(this);" value="" required=""></textarea>
                        <input class="form-check-input" type="radio" name="respuesta_turnado" id="respuesta" value="1" required>
                        <label class="form-check-label" for="respuesta">
                            Respuesta a Turnado
                        </label>
                        <input class="form-check-input" type="radio" name="respuesta_turnado" id="comentario" value="0" required>
                        <label class="form-check-label" for="comentario">
                            Comentario de Turno
                        </label>

                    </div>
                    <div class="col-md-4 px-1">
                        <label>Adjuntar Evidencia</label><br>
                        <input type="file" class="form-file-input" id="archivo_acuse" name="archivo_acuse">
                    </div>
                    <div class="col-md-4 px-1">
                        <label>Registra</label><br>
                        <input type="text" class="form-control" value="{{Session::get('nombre')}}" readonly="">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label></label><br>
                        <button type="submit" class="btn btn-primary round" id="guardar">Guardar</button>
                        <button type="reset" class="btn btn-danger round" onclick="history.back()">Cancelar</button>
                    </div>
                </div>
@endsection

{{-- botones de criticidad que se muestran en datatables --}}
@if($criterio == 'BAJO')
<span class="badge bg-success">BAJO</span>
@endif
@if($criterio == 'MEDIO')
<span class="badge bg-warning">MEDIO</span>
@endif
@if($criterio == 'CRITICO')
<span class="badge bg-danger">CRITICO</span>
@endif

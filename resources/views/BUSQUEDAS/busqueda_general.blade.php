@extends('principal')
@section('contenido')

    <div class="col-md-12">
        <div class="card card-user">
            <div class="card-header">
                <h5 class="card-title">Búsqueda de Turnos</h5>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group" id="_resultados">
                            <div class="table-responsive">
                                <table class="table table-striped" style="width:100%" id="general">
                                    <thead>
                                        <th>Turno</th>
                                        <th>No. de oficio</th>
                                        <th>Fecha de Oficio</th>
                                        <th>Fecha de recepción</th>
                                        <th>Año</th>
                                        <th>Asunto</th>
                                        <th>Remitente</th>
                                        <th>Turnado A</th>
                                        <th>C. copia</th>
                                        <th>Observaciones</th>
                                        <th>Atención</th>
                                        <th>Vigencia</th>
                                        <th>Estatus</th>
                                        <th>Opciones</th>
                                    </thead>
                                </table>
                                <!-- Responsive tables end -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .filters {
            width: 100%;
        }

    </style>
    <script src="{{ asset('/dist/assets/js/dataTables.bootstrap5.min.js') }}" defer></script>
    <script type="text/javascript">
        $(document).ready(function() {


            $('#general thead tr').clone(true).appendTo('#general thead');
            $('#general thead tr:eq(1) th').each(function(i) {
                var title = $(this).text();
                $(this).html('<input class="filters" id="celda' + i + '" type="text" placeholder="' +
                    title + '" />');

                $('input', this).on('keyup change', function() {
                    if (table.column(i).search() !== this.value) {
                        table
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
            });

            $('#celda8').hide();

            var table = $('#general').DataTable({

                "destroy": true,
                "serverSide": true,
                "orderCellsTop": true,
                "ajax": {
                    "url": "{{ url('general_lista_json') }}"
                },

                "columns": [{
                        data: 'turno',
                        name: 'recepciones.turno'
                    },
                    {
                        data: 'no_oficio',
                        name: 'recepciones.no_oficio'
                    },
                    {
                        data: 'fecha_oficio',
                        name: 'recepciones.fecha_oficio'
                    },
                    {
                        data: 'fecha_recepcion',
                        name: 'recepciones.fecha_general'
                    },
                    {
                        data: 'anio',
                        name: 'recepciones.anio'
                    },
                    {
                        data: 'asunto',
                        name: 'recepciones.asunto'
                    },
                    {
                        data: 'remitente',
                        name: 'remitentes.remitente'
                    },
                    {
                        data: 'turnado',
                        name: 'turnados.turnado'
                    },
                    {
                        data: 'ccp'
                    },
                    {
                        data: 'observaciones',
                        name: 'recepciones.observaciones'
                    },
                    {
                        data: 'prioritario',
                        name: 'prioritario'
                    },
                    {
                        data: 'vigencia',
                        name: 'recepciones.vigencia'
                    },
                    {
                        data: 'status',
                        name: 'status.status'
                    },
                    {
                        data: 'btn'
                    }
                ],
                "language": {
                    "info": "_TOTAL_ registros",
                    "search": "Buscar",
                    "paginate": {
                        "next": "Siguiente",
                        "previous": "Anterior",
                    },
                    "lengthMenu": 'Mostrar <select >' +
                        '<option value="10">10</option>' +
                        '<option value="30">30</option>' +
                        '<option value="-1">Todos</option>' +
                        '</select> registros',
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "emptyTable": "No hay datos",
                    "zeroRecords": "No hay coincidencias",
                    "infoEmpty": "",
                    "infoFiltered": ""
                },
                "drawCallback": function(settings) {
                    feather.replace();
                },
                "drawCallback": function(settings) {
                    feather.replace();
                },
                "dom": 'Blfrtip',
                "buttons": [{
                extend: 'excel',
                text: '<button type="button" class="btn btn-success round">Recepciones</button>',
                title: 'Recepciones-Excel',
            }, ],
            });
            console.log("prueba de fuego :V")
        });
    </script>

@stop

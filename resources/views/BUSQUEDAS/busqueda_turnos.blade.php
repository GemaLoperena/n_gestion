@extends('principal')
@section('contenido')
<div class="col-md-12">
    <div class="card card-user">
        <div class="card-header">
            <h5 class="card-title">Respuestas a Turnado</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group" id="resultados_turnos">
                        <div class="table-responsive">
                            <table class="table table-striped" style="width:100%" id="turnos">
                                <thead>
                                    <th>Turno</th>
                                    <th>Fecha de recepción</th>
                                    <th>No. oficio</th>
                                    <th>Asunto</th>
                                    <th>Remitente</th>
                                    <th>Observaciones</th>
                                    <th>Turnado a</th>
                                    <th>Con Copia Para</th>
                                    <th>Vigencia</th>
                                    <th>Estatus</th>
                                    <th>Situación</th>
                                    <th>Asignado</th>
                                    <th>Opciones</th>
                                </thead>
                            </table>
                            <!-- Responsive tables end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    #celda0 {
        width: 50px;
    }
    #celda1 {
        width: 100px;
    }
    #celda2 {
        width: 75px;
    }
    #celda3 {
        width: 200px;
    }
    #celda4 {
        width: 100px;
    }
    #celda5 {
        width: 100px;
    }
    #celda6 {
        width: 200px;
    }
    #celda7 {
        width: 200px;
    }
    #celda8 {
        width: 75px;
    }
    #celda9 {
        width: 75px;
    }
    #celda10 {
        width: 75px;
    }
    #celda11 {
        width: 75px;
    }
    #celda12 {
        width: 50px;
    }
</style>

<script src="{{asset('/dist/assets/js/dataTables.bootstrap5.min.js')}}" defer></script>
<script type="text/javascript">
    $(document).ready(function () {


        $('#turnos thead tr').clone(true).appendTo('#turnos thead');
        $('#turnos thead tr:eq(1) th').each(function (i) {
            var title = $(this).text();
            $(this).html('<input class="filters" id="celda' + i + '" type="text" placeholder="' +
                title + '" />');

            $('input', this).on('keyup change', function () {
                if (table.column(i).search() !== this.value) {
                    table
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });
        });

        $('#celda6 ').hide();
        $('#celda7').hide();
        $('#celda12').hide();

        var table = $('#turnos').DataTable({
            "destroy": true,
            "serverSide": true,
            "orderCellsTop": true,
            "autoWidth": false,
            "ajax": {
                "url": "{{ url('turnos_lista_json') }}"
            },
            "columns": [{
                    data: 'turno',
                    name: 'recepciones.turno'
                },
                {
                    data: 'fecha_recepcion',
                    name: 'recepciones.fecha_recepcion'
                },
                {
                    data: 'no_oficio',
                    name: 'recepciones.no_oficio'
                },
                {
                    data: 'asunto',
                    name: 'recepciones.asunto'
                },
                {
                    data: 'remitente',
                    name: 'remitentes.remitente'
                },
                {
                    data: 'observaciones',
                    name: 'recepciones.observaciones'
                },
                {
                    data: 'turnado'
                },
                {
                    data: 'ccp'
                },
                {
                    data: 'vigencia',
                    name: 'recepciones.vigencia'
                },
                {
                    data: 'status',
                    name: 'status.status'
                },
                {
                    data: 'situacion',
                    name: 'recepciones.situacion'
                },
                {
                    data: 'nom_usuario',
                    name: 'usuarios.nom_usuario'
                },
                {
                    data: 'btn'
                }
            ],
            "aoColumnDefs": [{
                "aTargets": [10],
                "mData": "download_link",
                "mRender": function (data, type, full) {
                    if (data === "1") {
                        return 'Abierto';
                    } else {
                        return 'Cerrado';
                    }
                }
            }],
            "language": {
                "info": "_TOTAL_ registros",
                "search": "Buscar",
                "paginate": {
                    "next": "Siguiente",
                    "previous": "Anterior",
                },
                "lengthMenu": 'Mostrar <select >' +
                    '<option value="10">10</option>' +
                    '<option value="30">30</option>' +
                    '<option value="-1">Todos</option>' +
                    '</select> registros',
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "emptyTable": "No hay datos",
                "zeroRecords": "No hay coincidencias",
                "infoEmpty": "",
                "infoFiltered": ""
            },
            "drawCallback": function (settings) {
                feather.replace();
            },
            "dom": 'Blfrtip',

            "buttons": [{
                extend: 'excel',
                text: '<button type="button" class="btn btn-success round">Respuestas a turnado</button>',
                title: 'Respuestas_a_Turnado-Excel',
            }, ],
        });

    });

</script>
@stop

@extends('principal')
@section('contenido')
<div class="col-md-12">
    <div class="card card-user">
        <div class="card-header">
            <h5 class="card-title">C.C.P</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group" id="resultados_ccp">
                        <div class="table-responsive">
                            <table class="table table-striped" id="tabla_ccp">
                                    <thead>
                                        <th>Turno</th>
                                        <th>Número de oficio</th>
                                        <th>Fecha de Oficio</th>
                                        <th>Fecha de recepción</th>
                                        <th>Hora de recepción</th>
                                        <th>Asunto</th>
                                        <th>Remitente</th>
                                        <th>Observaciones</th>
                                        <th>Turnado A</th>
                                        <th>C.C.P</th>
                                        <th>Vigencia</th>
                                        <th>Estatus</th>
                                        <th>Más</tr>
                                    </thead>
                                </table>
                            <!-- Responsive tables end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    #celda0 {
        width: 50px;
    }
    #celda1 {
        width: 75px;
    }
    #celda2 {
        width: 100px;
    }
    #celda3 {
        width: 100px;
    }
    #celda4 {
        width: 100px;
    }
    #celda5 {
        width: 200px;
    }
    #celda6 {
        width: 100px;
    }
    #celda7 {
        width: 200px;
    }
    #celda8 {
        width: 75px;
    }
    #celda9 {
        width: 75px;
    }
    #celda10 {
        width: 75px;
    }
    #celda11 {
        width: 75px;
    }
    #celda12 {
        width: 50px;
    }
</style>

<script src="{{asset('/dist/assets/js/dataTables.bootstrap5.min.js')}}" defer></script>
<script type="text/javascript">
    $(document).ready(function() {
   
   
        $('#tabla_ccp thead tr').clone(true).appendTo( '#tabla_ccp thead' );
           $('#tabla_ccp thead tr:eq(1) th').each( function (i) {
           var title = $(this).text();
           $(this).html( '<input class="filters" id="celda'+i+'" type="text" placeholder="'+title+'" />' );

           $( 'input', this ).on( 'keyup change', function () {
               if ( table.column(i).search() !== this.value ) {
                   table
                       .column(i)
                       .search( this.value )
                       .draw();
                   }
               } );
           });
   
           $('#celda8').hide();
           $('#celda9').hide();
           $('#celda12').hide();
   
           var table = $('#tabla_ccp').DataTable( {
                  "destroy": true,
                  "serverSide": true,
                  "orderCellsTop": true,
                  "autoWidth": false,
                   "ajax": {
                   "url": "{{ url('ccp_lista_json') }}"
                   },
                   "columns": [
                        {data: 'turno', name: 'recepciones.turno'},
                        {data: 'no_oficio', name: 'recepciones.no_oficio'},
                        {data: 'fecha_oficio', name: 'recepciones.fecha_oficio'},
                        {data: 'fecha_recepcion', name: 'recepciones.fecha_recepcion'},
                        {data: 'hora_recepcion', name: 'recepciones.hora_recepcion'},
                        {data: 'asunto', name: 'recepciones.asunto'},
                        {data: 'remitente', name: 'remitentes.remitente'},
                        {data: 'observaciones', name: 'recepciones.turno'},
                        {data: 'turnado'},
                        {data: 'ccp'},
                        {data: 'vigencia', name: 'recepciones.vigencia'},
                        {data: 'status', name: 'status.status'},
                        {data: 'btn'}
                    ],
                   "language": {
                       "info": "_TOTAL_ registros",
                       "search": "Buscar",
                       "paginate": {
                           "next": "Siguiente",
                           "previous": "Anterior",
                       },
                       "lengthMenu": 'Mostrar <select >'+
                                   '<option value="10">10</option>'+
                                   '<option value="30">30</option>'+
                                   '<option value="-1">Todos</option>'+
                                   '</select> registros',
                       "loadingRecords": "Cargando...",
                       "processing": "Procesando...",
                       "emptyTable": "No hay datos",
                       "zeroRecords": "No hay coincidencias",
                       "infoEmpty": "",
                       "infoFiltered": ""
                   },
                   "drawCallback": function( settings ) {
                       feather.replace();
                   }
           });
   
       });
   
   </script>

@stop

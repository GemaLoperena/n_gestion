@extends('principal')
@section('contenido')
<div class="col-md-12">
    <div class="card card-user">
        <div class="card-header">
            <h5 class="card-title">Búsqueda de Turnos</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group" id="resultados_recepciones">
                        <div class="table-responsive">
                            <table class="table table-striped" style="width:100%" id="recepcion">
                                <thead>
                                    <th>Turno</th>
                                    <th>Número de oficio</th>
                                    <th>Año</th>
                                    <th>Fecha de Oficio</th>
                                    <th>Fecha de recepción</th>
                                    <th>Hora de recepción</th>
                                    <th>Asunto</th>
                                    <th>Criterio</th>
                                    <th>Remitente</th>
                                    <th>Registró</th>
                                    <th>Opciones</th>
                                </thead>
                            </table>
                            <!-- Responsive tables end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    #celda0 {
        width: 50px;
    }
    #celda1 {
        width: 50px;
    }
    #celda2 {
        width: 50px;
    }
    #celda3 {
        width: 100px;
    }
    #celda4 {
        width: 100px;
    }
    #celda5 {
        width: 100px;
    }
    #celda6 {
        width: 200px;
    }
    #celda7 {
        width: 75px;
    }
    #celda8 {
        width: 75px;
    }
    #celda9 {
        width: 75px;
    }
    #celda10 {
        width: 75px;
    }
</style>


<script src="{{asset('/dist/assets/js/dataTables.bootstrap5.min.js')}}" defer></script>
<script type="text/javascript">
    $(document).ready(function () {


        $('#recepcion thead tr').clone(true).appendTo('#recepcion thead');
        $('#recepcion thead tr:eq(1) th').each(function (i) {
            var title = $(this).text();
            $(this).html('<input class="filters" id="celda' + i + '" type="text" placeholder="' +
                title + '" />');

            $('input', this).on('keyup change', function () {
                if (table.column(i).search() !== this.value) {
                    table
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });
        });

        $('#celda7').hide();
        $('#celda10').hide();

        var table = $('#recepcion').DataTable({

            "destroy": true,
            "serverSide": true,
            "orderCellsTop": true,
            "ajax": {
                "url": "{{ url('recepcion_lista_json') }}"
            },

            "columns": [{
                    data: 'turno',
                    name: 'recepciones.turno'
                },
                {
                    data: 'no_oficio',
                    name: 'recepciones.no_oficio'
                },
                {
                    data: 'anio',
                    name: 'recepciones.anio'
                },
                {
                    data: 'fecha_oficio',
                    name: 'recepciones.fecha_oficio'
                },
                {
                    data: 'fecha_recepcion',
                    name: 'recepciones.fecha_recepcion'
                },
                {
                    data: 'hora_recepcion',
                    name: 'recepciones.hora_recepcion'
                },
                {
                    data: 'asunto',
                    name: 'recepciones.asunto'
                },
                {
                    data: 'btn_c'
                },
                {
                    data: 'remitente',
                    name: 'remitentes.remitente'
                },
                {
                    data: 'nom_usuario',
                    name: 'usuarios.nom_usuario'
                },
                {
                    data: 'btn'
                }
            ],
            "language": {
                "info": "_TOTAL_ registros",
                "search": "Buscar",
                "paginate": {
                    "next": "Siguiente",
                    "previous": "Anterior",
                },
                "lengthMenu": 'Mostrar <select >' +
                    '<option value="10">10</option>' +
                    '<option value="30">30</option>' +
                    '<option value="-1">Todos</option>' +
                    '</select> registros',
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "emptyTable": "No hay datos",
                "zeroRecords": "No hay coincidencias",
                "infoEmpty": "",
                "infoFiltered": ""
            },
            "drawCallback": function (settings) {
                feather.replace();
            },
            "drawCallback": function (settings) {
                feather.replace();
            },
            "dom": 'Blfrtip',

            "buttons": [{
                extend: 'excel',
                text: '<button type="button" class="btn btn-success round">Recepciones</button>',
                title: 'Recepciones-Excel',
            }, ],
        });
        console.log("prueba de fuego :V")
    });

</script>

@stop

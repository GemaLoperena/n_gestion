@extends('principal')
@section('contenido')

<div class="col-md-12">
    <div class="card card-user">
        <div class="card-header">
            @if ($message = Session::get('success'))
            <div class="alert alert-success col-md-12" role="alert">{{ $message }}
            </div>
            @endif
            @if ($message = Session::get('warning'))
            <div class="alert alert-warning col-md-12" role="alert">{{ $message }}
            </div>
            @endif
            <h5 class="card-title">Alta de nuevo usuario</h5>
        </div>
        <div class="card-body">
            <form action="{{route('guarda_usuarios')}}" method="POST">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-4 pr-1">
                        <div class="form-group">
                            <label>Nombre(s):</label>
                            <input type="text" class="form-control form-control-sm" name="nom_usuario"
                                onKeyup="Upper(this);" required="">
                        </div>
                    </div>
                    <div class="col-md-4 px-1">
                        <div class="form-group">
                            <label>Apellido Paterno:</label>
                            <input type="text" class="form-control form-control-sm" name="app_usuario"
                                onKeyup="Upper(this);" required="">
                        </div>
                    </div>
                    <div class="col-md-4 pl-1">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Apellido Materno:</label>
                            <input type="text" class="form-control form-control-sm" name="apm_usuario"
                                onKeyup="Upper(this);" required="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 pr-1">
                        <div class="form-group">
                            <label>Profesión:</label>
                            <input type="text" class="form-control form-control-sm" name="profesion"
                                onKeyup="Upper(this);" required="">
                        </div>
                    </div>
                    <div class="col-md-3 px-1">
                        <div class="form-group">
                            <label>Contraseña:</label>
                            <input type="password" class="form-control form-control-sm" id="contrasena"
                                onKeyup="Upper(this);" required="">
                        </div>
                    </div>
                    <div class="col-md-3 px-1">
                        <div class="form-group">
                            <label>Repetir Contraseña:</label>
                            <input type="password" class="form-control form-control-sm" name="contrasena1"
                                id="contrasena1" style="text-transform:uppercase" onKeyup="verificarPasswords()"
                                required="">
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                        <label>Activo: </label>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="activo" id="optionsRadios1" value="1" checked="">
                                    Si </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                        <label>&nbsp;</label>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="activo" id="optionsRadios1" value="0">
                                    No </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-1">
                        <div class="form-group">
                            <label>Correo:</label>
                            <input type="text" class="form-control form-control-sm" name="usuario" required="">
                        </div>
                    </div>
                    <div class="col-md-4 px-1">
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Dirección:</label>
                            <select class="form-control form-control-sm" name="id_direccion" required="">
                                <option value="">SELECCIONE UNA DIRECCIÓN</option>
                                @foreach($direcciones as $d)
                                <option value="{{$d->id_direccion}}">{{$d->direccion}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 pl-1">
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Tipo de usuario:</label>
                            <select class="form-control form-control-sm" name="id_rol" required="">
                                <option value="">SELECCIONE UN TIPO DE USUARIO</option>
                                @foreach($roles as $r)
                                <option value="{{$r->id_rol}}">{{$r->rol}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="typography-line" id="incorrecto" style="display: none;">
                        <p class="text-danger">Las contraseñas son diferentes, intente nuevamente</p>
                    </div>
                    <div class="typography-line" id="correcto" style="display: none;">
                        <p class="text-success">Las contraseñas coinciden, puede continuar</p>
                    </div>
                    <div class="update ml-auto mr-auto">
                        <button type="submit" class="btn btn-primary round" id="guardar">Guardar Usuario</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop

@extends('principal')
@section('contenido')

<div class="col-md-12">
    <div class="card card-user">
        <div class="card-header">
            @if ($message = Session::get('success'))
            <div class="alert alert-success col-md-12" role="alert">{{ $message }}
            </div>
            @endif
            @if ($message = Session::get('warning'))
            <div class="alert alert-warning col-md-12" role="alert">{{ $message }}
            </div>
            @endif
            <h5 class="card-title">Búsqueda de usuarios</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group" id="resultados_usuarios">
                        <!-- Muestra de resultados -->
                        <!-- table responsive -->

                        <div class="table-responsive">
                            <table class="table table-striped" style="width:100%" id="usuarios">
                                <thead>
                                    <th>ID</th>
                                    <th>Nombre</th>
                                    <th>Activo</th>
                                    <th>Dirección</th>
                                    <th>Tipo de usuario</th>
                                    <th>Opciones</th>
                                </thead>
                            </table>
                            <!-- Responsive tables end -->
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('/dist/assets/js/dataTables.bootstrap5.min.js')}}" defer></script>
<script type="text/javascript">
 $(document).ready(function() {


        $('#usuarios thead tr').clone(true).appendTo( '#usuarios thead' );
        $('#usuarios thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input id="celda'+i+'" type="text" placeholder="Buscar '+title+'" />' );
 
        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
                }
            } );
        });

        $('#celda2').hide();
        $('#celda5').hide();

        var table = $('#usuarios').DataTable( {
                "destroy": true,
                "serverSide": true,
                "orderCellsTop": true,
                "fixedHeader": true,
                "ajax": {
                "url": "{{ url('usuarios_lista_json') }}"
                },
                "columns": [
                    {data: 'id_usuario', name: 'usuarios.id_usuario'},
                    {data: 'nom_usuario', name: 'usuarios.nom_usuario'},
                    {data: 'activo', name: 'usuarios.activo'},
                    {data: 'direccion', name: 'direcciones.direccion'},
                    {data: 'rol', name: 'roles.rol'},
                    {data: 'btn'}
                ],
                "aoColumnDefs": [ 
                    {
                        "aTargets": [ 2 ],
                        "mData": "download_link",
                        "mRender": function ( data, type, full ) {
                            if(data === 1){
                                return 'Activo';
                            }else{
                                return 'Inactivo';
                            }
                        }
                    }
                ],
                "language": {
                    "info": "_TOTAL_ registros",
                    "search": "Buscar",
                    "paginate": {
                        "next": "Siguiente",
                        "previous": "Anterior",
                    },
                    "lengthMenu": 'Mostrar <select >'+
                                '<option value="10">10</option>'+
                                '<option value="30">30</option>'+
                                '<option value="-1">Todos</option>'+
                                '</select> registros',
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "emptyTable": "No hay datos",
                    "zeroRecords": "No hay coincidencias",
                    "infoEmpty": "",
                    "infoFiltered": ""
                },
                "drawCallback": function( settings ) {
                    feather.replace();
                },
            "dom": 'Blfrtip',

            "buttons": [{
                extend: 'excel',
                text: '<button type="button" class="btn btn-success round">Búsqueda de Usuarios</button>',
                title: 'Usuarios-Excel',
            }, ],
        });

    });

</script>
@stop
@extends('principal')
@section('contenido')

<div class="col-md-12">
    <div class="card card-user">
        <div class="card-header">
            @if ($message = Session::get('success'))
            <div class="alert alert-success col-md-12" role="alert">{{ $message }}
            </div>
            @endif
            @if ($message = Session::get('warning'))
            <div class="alert alert-warning col-md-12" role="alert">{{ $message }}
            </div>
            @endif
            <h5 class="card-title">Consulta de Directivos</h5>
            <button type="button" class="btn btn-success round" id="alta_nuevo_directivo">Alta de
                nuevo
                directivo</button>

        </div>

        <div class="card-body" hidden="" id="alta_directivos">
            <form action="{{route('alta_directivos')}}" method="POST">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-5 pr-1">
                        <div class="form-group">
                            <label>Nombre del directivo:</label>
                            <input type="text" class="form-control form-control-sm" name="nombre" onKeyup="Upper(this);"
                                required="">
                        </div>
                    </div>
                    <div class="col-md-7 pl-1">
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Dirección:</label>
                            <select class="form-control form-control-sm" name="id_direccion" required="">
                                <option value="">SELECCIONE UNA DIRECCIÓN</option>
                                @foreach($direcciones as $d)
                                <option value="{{$d->id_direccion}}">{{$d->direccion}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-10 pr-1">
                        <div class="form-group">
                            <label></label>
                            <button type="submit" class="btn btn-primary round">Guardar Dirección</button>
                            <button type="button" class="btn btn-danger round" id="cancelar_directivo">Cancelar</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group" id="resultados_usuarios">
                        <div class="table-responsive">
                            <table class="table table-striped"  style="width:100%" id="direcciones">
                                    <thead>
                                        <th>ID</th>
                                        <th>Nombre</th>
                                        <th>Direccion</th>
                                        <th>Activo</th>
                                        <th>Opciones</th>
                                    </thead>
                                </table>
                            <!-- Responsive tables end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>



<style>
    .filters{
      width: 100%;
    }
    </style>
    
<script src="{{asset('/dist/assets/js/dataTables.bootstrap5.min.js')}}" defer></script>
<script type="text/javascript">
     $(document).ready(function() {
    
    
            $('#direcciones thead tr').clone(true).appendTo( '#direcciones thead' );
            $('#direcciones thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            $(this).html( '<input class="filters" id="celda'+i+'" type="text" placeholder="'+title+'" />' );
     
            $( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                    }
                });
            });
    
            $('#celda4').hide();
    
            var table = $('#direcciones').DataTable( {
                    "destroy": true,
                    "serverSide": true,
                    "orderCellsTop": true,
                    "ajax": {
                    "url": "{{ url('directivos_lista_json') }}"
                    },
                   
                    "columns": [
                        {data: 'id_directivo', name: 'directivos.id_directivo'},
                        {data: 'nombre', name: 'directivos.nombre'},
                        {data: 'direccion', name: 'direcciones.direccion'},
                        {data: 'activo', name: 'directivos.activo'},
                        {data: 'btn'}
                    ],
                    "aoColumnDefs": [ 
                    {
                        "aTargets": [ 3 ],
                        "mData": "download_link",
                        "mRender": function ( data, type, full ) {
                            if(data === 1){
                                return 'Activo';
                            }else{
                                return 'Inactivo';
                            }
                        }
                    }
                    ],
                    "language": {
                        "info": "_TOTAL_ registros",
                        "search": "Buscar",
                        "paginate": {
                            "next": "Siguiente",
                            "previous": "Anterior",
                        },
                        "lengthMenu": 'Mostrar <select >'+
                                    '<option value="10">10</option>'+
                                    '<option value="30">30</option>'+
                                    '<option value="-1">Todos</option>'+
                                    '</select> registros',
                        "loadingRecords": "Cargando...",
                        "processing": "Procesando...",
                        "emptyTable": "No hay datos",
                        "zeroRecords": "No hay coincidencias",
                        "infoEmpty": "",
                        "infoFiltered": ""
                    },
                    "drawCallback": function( settings ) {
                        feather.replace();
                    },
            "dom": 'Blfrtip',
            "buttons": [{
                extend: 'excel',
                text: '<button type="button" class="btn btn-warning round" >Reporte de Directivos</button>',
                title: 'Directivos-Excel',
            }, ],
            });
    
        });
    
    </script>
@stop

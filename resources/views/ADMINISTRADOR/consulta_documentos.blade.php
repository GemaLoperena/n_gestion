@extends('principal')
@section('contenido')

<div class="col-md-12">
    <div class="card card-user">
        <div class="card-header">
            @if ($message = Session::get('success'))
            <div class="alert alert-success col-md-12" role="alert">{{ $message }}
            </div>
            @endif
            @if ($message = Session::get('warning'))
            <div class="alert alert-warning col-md-12" role="alert">{{ $message }}
            </div>
            @endif
            <h5 class="card-title">Consulta de Documentos</h5>
            <button type="button" class="btn btn-success round" id="alta_documentos">Alta de Nuevo Documento</button>
        </div>
        <div class="card-body" hidden="" id="alta_documento">
            <form action="{{route('alta_documentos')}}" method="POST">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-10 pr-1">
                        <div class="form-group">
                            <label>Nombre del nuevo Documento:</label>
                            <input type="text" class="form-control" name="descripcion" onKeyup="Upper(this);" required="">
                        </div>
                    </div>
                    <div class="col-md-10 pr-1">
                        <div class="form-group">
                            <label></label>
                            <button type="submit" class="btn btn-primary round">Guardar Documento</button>
                            <button type="button" class="btn btn-danger round" id="cancelar_documento">Cancelar</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped"  style="width:100%" id="documentos">
                        <thead>
                            <th>ID</th>
                            <th>Documento</th>
                            <th>Opciones</th>
                        </thead>
                </table>
                <!-- Responsive tables end -->
            </div>
        </div>
    </div>
</div>

<style>
.filters{
width: 100%;
}
</style>

<script src="{{asset('/dist/assets/js/dataTables.bootstrap5.min.js')}}" defer></script>
<script type="text/javascript">
$(document).ready(function() {


    $('#documentos thead tr').clone(true).appendTo( '#documentos thead' );
    $('#documentos thead tr:eq(1) th').each( function (i) {
    var title = $(this).text();
    $(this).html( '<input class="filters" id="celda'+i+'" type="text" placeholder="'+title+'" />' );

    $( 'input', this ).on( 'keyup change', function () {
        if ( table.column(i).search() !== this.value ) {
            table
                .column(i)
                .search( this.value )
                .draw();
            }
        } );
    });

    $('#celda3').hide();

    var table = $('#documentos').DataTable( {
            "destroy": true,
            "serverSide": true,
            "orderCellsTop": true,
            "ajax": {
            "url": "{{ url('documentos_lista_json') }}"
            },
           
            "columns": [
                {data: 'id_tdocumento', name: 'id_tdocumento'},
                {data: 'descripcion', name: 'descripcion'},
                
                {data: 'btn'}
            ],
            "language": {
                "info": "_TOTAL_ registros",
                "search": "Buscar",
                "paginate": {
                    "next": "Siguiente",
                    "previous": "Anterior",
                },
                "lengthMenu": 'Mostrar <select >'+
                            '<option value="10">10</option>'+
                            '<option value="30">30</option>'+
                            '<option value="-1">Todos</option>'+
                            '</select> registros',
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "emptyTable": "No hay datos",
                "zeroRecords": "No hay coincidencias",
                "infoEmpty": "",
                "infoFiltered": ""
            },
            "drawCallback": function( settings ) {
                feather.replace();
            },
            "dom": 'Blfrtip',
            "buttons": [{
                extend: 'excel',
                text: '<button type="button" class="btn btn-warning round" >Reporte de Documentos</button>',
                title: 'Documentos-Excel',
            }, ],
    });

});

</script>
@stop

@extends('principal')
@section('contenido')
@if ($message = Session::get('success'))
<div class="alert alert-success col-md-12" role="alert">{{ $message }}
</div>
@endif
@if ($message = Session::get('warning'))
<div class="alert alert-warning col-md-12" role="alert">{{ $message }}
</div>
@endif
<div class="col-md-12">
    <div class="card card-user">
        <div class="card-body">
        <h5 class="card-title">Actualización de Recepción</h5>
            <form action="{{route('guarda_actualizacion')}}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-12 pr-1" hidden>
                        <div class="form-group">
                            <label>id:</label>
                            <input type="text" class="form-control" name="id_recepcion"
                                value="{{$consulta->id_recepcion}}" readonly="">
                        </div>
                    </div>
                    <div class="col-md-12 pr-1" hidden>
                        <div class="form-group">
                            <label>Turno:</label>
                            <input type="text" class="form-control" name="anio"
                                value="{{$consulta->anio}}" readonly="">
                        </div>
                    </div>
                    <div class="col-md-1 pr-1">
                        <div class="form-group">
                            <label>Turno:</label>
                            <input type="text" class="form-control" name="turno"
                                value="{{$consulta->turno}}" readonly="">
                        </div>
                    </div>
                    <div class="col-md-2 px-1">
                        <div class="form-group">
                            <label>Número de oficio:</label>
                            <input type="text" class="form-control" name="no_oficio"
                                value="{{$consulta->no_oficio}}" onKeyup="Upper(this);" required="">
                        </div>
                    </div>
                    <div class="col-md-2 px-1">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Fecha de oficio:</label>
                            <input type="date" class="form-control" name="fecha_oficio"
                                value="{{$consulta->fecha_oficio}}" required max="<?php $hoy=date("Y-m-d"); echo $hoy;?>">
                        </div>
                    </div>
                    <div class="col-md-2 px-1">
                        <div class="form-group">
                            <label>Fecha de recepción:</label>
                            <input type="date" class="form-control" name="fecha_recepcion"
                                value="{{$consulta->fecha_recepcion}}" required max="<?php $hoy1=date("Y-m-d"); echo $hoy1;?>">
                        </div>
                    </div>
                    <div class="col-md-1 pl-1">
                        <div class="form-group">
                            <label>Hora:</label>
                            <input type="datetime" class="form-control" name="hora_recepcion"
                                value="{{$consulta->hora_recepcion}}" readonly>
                        </div>
                    </div>
                    @if($consulta->situacion == 1)
                    <div class="col-md-1 pr-1">
                        <label>Situación:</label>
                        <div class="form-check">
                            <label>Abierto</label>
                            <input class="form-check-input" type="radio" value="1" name="situacion" id="estado1" checked>
                        </div>
                        <div class="form-check">
                            <label>Cerrado</label>
                            <input class="form-check-input" type="radio" value="0" name="situacion" id="estado1">
                        </div>
                    </div>
                    @endif
                    @if($consulta->situacion == 0)
                    <div class="col-md-1 pr-1">
                        <label>Situacion:</label>
                        <div class="form-check">
                            <label>Abierto</label>
                            <input class="form-check-input" type="radio" value="1" name="situacion" id="estado1" >
                        </div>
                        <div class="form-check">
                            <label>Cerrado</label>
                            <input class="form-check-input" type="radio" value="0" name="situacion" id="estado1" checked>
                        </div>
                    </div>
                    @endif
                    <div class="col-md-3 pl-1">
                        <div class="form-group">
                            <label>Registró:</label>
                            <input type="text" class="form-control" name="id_usuario"
                                value="{{$consulta->id_usuario}}" hidden>
                            <input type="text" class="form-control" value="{{$consulta->nom_usuario}}"
                                readonly="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 pr-1">
                        <div class="form-group form-label-group">
                            <label for="label-textarea">Asunto:</label>
                            <textarea class="form-control" id="label-textarea" onKeyup="Upper(this);" name="asunto"
                                rows="3">{{$consulta->asunto}}</textarea>
                            <label for="label-textarea"></label>
                        </div>
                    </div>
                    @if($consulta->id_status == 1)
                    <div class="col-md-6 pr-1">
                        <div class="form-group form-label-group">
                            <label for="label-textarea">Observaciones:</label>
                            <textarea class="form-control" id="label-textarea" name="observaciones" onKeyup="Upper(this);"
                                rows="3">{{$consulta->observaciones}}</textarea>
                        </div>
                    </div>
                    @endif
                    @if($consulta->id_status == 2)
                    <div class="col-md-6 pr-1">
                        <div class="form-group form-label-group">
                            <label for="label-textarea">Observaciones:</label>
                            <textarea class="form-control" id="label-textarea" name="observaciones" rows="3" onKeyup="Upper(this);"
                                readonly>{{$consulta->observaciones}}</textarea>
                        </div>
                    </div>
                    @endif
                </div>
                <div class="row">
                    <div class="col-md-2 pr-1">
                        <div class="form-group">
                            <label>Estatus del turno:</label>
                            <select class="form-select" id="basicSelect" name="id_status">
                                @foreach($status as $s)
                                    @if($consulta->id_status == $s->id_status)
                                        <option value="{{$s->id_status}}" selected>{{$s->status}}</option>
                                    @else
                                        <option value="{{$s->id_status}}">{{$s->status}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 pr-1">
                        <div class="form-group">
                            <label for="remitente">Remitente:</label>
                                <select name="remitente" class="form-select" id="basicSelect" required>
                                    <option value="">Remitente</option>
                                    @foreach ($remitentes as $remitente)
                                        @if($remitente->id_remitente == $consulta->id_remitente)
                                            <option value="{{ $remitente->id_remitente }}" selected>{{ $remitente->remitente }}</option>
                                        @else
                                            <option value="{{ $remitente->id_remitente }}">{{ $remitente->remitente }}</option>
                                        @endif
                                    @endforeach
                                </select>
                        </div>
                    </div>
                    @if($consulta->r_conjunta == 1)
                    <div class="col-md-2 pr-1">
                        <label>Respuesta conjunta:</label>
                        <div class="form-check">
                            <label>Concluye</label>
                            <input class="form-check-input" type="radio" value="1" name="r_conjunta" id="r_conjunta1" checked>
                        </div>
                        <div class="form-check">
                            <label>No concluye</label>
                            <input class="form-check-input" type="radio" value="0" name="r_conjunta" id="r_conjunta1">
                        </div>
                    </div>
                    @endif
                    @if($consulta->r_conjunta == 0)
                    <div class="col-md-2 pr-1">
                        <label>Respuesta Conjunta:</label>
                        <div class="form-check">
                            <label>Concluye</label>
                            <input class="form-check-input" type="radio" value="1" name="r_conjunta" id="r_conjunta1">
                        </div>
                        <div class="form-check">
                            <label>No concluye</label>
                            <input class="form-check-input" type="radio" value="0" name="r_conjunta" id="r_conjunta1" checked>
                        </div>
                    </div>
                    @endif
                    @if($consulta->prioritario == 1)
                    <div class="col-md-2 pr-1">
                        <label>Atención:</label>
                        <div class="form-check">
                            <label>Pioritaria</label>
                            <input class="form-check-input" type="radio" value="1" name="atencion" id="atencion1" checked>
                        </div>
                        <div class="form-check">
                            <label>Ordinaria</label>
                            <input class="form-check-input" type="radio" value="0" name="atencion" id="atencion1">
                        </div>
                    </div>
                    @endif
                    @if($consulta->prioritario == 0)
                    <div class="col-md-2 pr-1">
                        <label>Atención:</label>
                        <div class="form-check">
                            <label>Pioritaria</label>
                            <input class="form-check-input" type="radio" value="1" name="atencion" id="atencion1">
                        </div>
                        <div class="form-check">
                            <label>Ordinaria</label>
                            <input class="form-check-input" type="radio" value="0" name="atencion" id="atencion1" checked>
                        </div>
                    </div>
                    @endif
                    @if($consulta->id_nomenclatura != 3)
                    <div class="col-md-2 pr-1">
                        <label>Procedencia:</label>
                        <div class="form-check">
                            <label>Oficialia de partes</label>
                            <input class="form-check-input" type="radio" name="flexRadioDisabled" id="flexRadioDisabled" disabled="">
                        </div>
                        <div class="form-check">
                            <label>Asesoria</label>
                            <input class="form-check-input" type="radio" name="flexRadioDisabled" id="flexRadioDisabled" disabled="">
                        </div>
                    </div>
                    @else
                    @if($consulta->procedencia == 1)
                    <div class="col-md-2 pr-1">
                        <label>Procedencia:</label>
                        <div class="form-check">
                            <label>Oficialia de partes</label>
                            <input class="form-check-input" type="radio" value="1" name="procedencia" id="flexRadioDisabled" checked>
                        </div>
                        <div class="form-check">
                            <label>Asesoria</label>
                            <input class="form-check-input" type="radio" value="0" name="procedencia" id="flexRadioDisabled" >
                        </div>
                    </div>
                    @endif
                    @if($consulta->procedencia == 0)
                    <div class="col-md-2 pr-1">
                        <label>Procedencia:</label>
                        <div class="form-check">
                            <label>Oficialia de partes</label>
                            <input class="form-check-input" type="radio" value="1" name="procedencia" id="flexRadioDisabled" >
                        </div>
                        <div class="form-check">
                            <label>Asesoria</label>
                            <input class="form-check-input" type="radio" value="0" name="procedencia" id="flexRadioDisabled" checked>
                        </div>
                    </div>
                    @endif
                    @endif
                </div>
                <div class="row">
                    @if(isset($consulta->archivo))
                    <div class="col-md-2 pr-1">
                        <div class="form-group">
                            <label>Archivo:</label><br>
                            <input type="text" name="archivo_n" value="{{ $consulta->archivo }}" hidden>
                            <a  href="{{ route('download',['archivo' => $consulta->archivo]) }}"><i data-feather="download" width="30" ></i>Descarga</a>
                            <a  href="{{ route('eliminar_archivo',['archivo' => $consulta->archivo]) }}"><i data-feather="delete" width="30" ></i>Eliminar</a>
                        </div>
                    </div>
                    @endif
                    @if(empty($consulta->archivo))
                    <div class="col-md-2 pr-1">
                        <div class="form-group">
                            <label>Archivo:</label>
                            <input type="file" class="form-control" name="archivo" onKeyup="Upper(this);">
                        </div>
                    </div>
                    @endif
                    <div class="col-md-2 pr-1">
                        <div class="form-group">
                            <label>Vigencia:</label>
                            <input type="date" class="form-control" name="vigencia" value="{{$consulta->vigencia}}" required min="<?php $hoy2=date("Y-m-d"); echo $hoy2;?>">
                        </div>
                    </div>
                    <div class="col-md-2 pr-1">
                        <div class="form-group">
                            <label>Nomenclatura:</label>
                            <input type="text" class="form-control" name="nomenclatura" value="{{$consulta->descripcion}}" readonly>
                        </div>
                    </div>
                    <div class="col-md-2 pr-1">
                        <div class="form-group">
                            <label>Número de expediente:</label>
                            <input type="text" class="form-control" name="expediente" value="{{$consulta->no_expediente}}" readonly>
                        </div>
                    </div>
                    <div class="col-md-2 pr-1">
                        <div class="form-group">
                            <label for="id_criterio">Criterio:</label>
                                <select name="id_criterio" class="form-select" id="basicSelect" required>
                                    <option value="">Remitente</option>
                                    @foreach ($criterios as $c)
                                        @if($c->id_criterio == $consulta->id_criterio)
                                            <option value="{{ $c->id_criterio }}" selected>{{ $c->criterio }}</option>
                                        @else
                                            <option value="{{ $c->id_criterio }}">{{ $c->criterio }}</option>
                                        @endif
                                    @endforeach
                                </select>
                        </div>
                    </div> 
                </div>
                <div class="row">
                    <div class="update ml-auto mr-auto">
                        <button type="submit" class="btn btn-primary round" id="guardar">Actualiza Recepción</button>
                        <button type="reset" class="btn btn-danger round" onclick="history.back()">Cancelar</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
@stop

@extends('principal')
@section('contenido')

<div class="col-md-12">
    <div class="card card-user">
        <div class="card-header">
            @if ($message = Session::get('success'))
            <div class="alert alert-success col-md-12" role="alert">{{ $message }}
            </div>
            @endif
            @if ($message = Session::get('warning'))
            <div class="alert alert-warning col-md-12" role="alert">{{ $message }}
            </div>
            @endif
            <h5 class="card-title">Modificación de Directivos</h5>
        </div>
        <div class="card-body">
            <form action="{{route('actualiza_directivos')}}" method="POST">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-2 pr-1">
                        <div class="form-group">
                            <label>ID de la directivo:</label>
                            <input type="text" class="form-control form-control-sm" name="id_directivo" value="{{$consulta->id_directivo}}" onKeyup="Upper(this);" readonly>
                        </div>
                    </div>
                    <div class="col-md-3 px-1">
                        <div class="form-group">
                            <label>Nombre de la dirección:</label>
                            <input type="text" class="form-control form-control-sm" name="nombre" value="{{$consulta->nombre}}"
                                onKeyup="Upper(this);" required="">
                        </div>
                    </div>
                    <div class="col-md-7 pl-1">
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Dirección:</label>
                            <select class="form-control form-control-sm" name="id_direccion" required="">
                                <option value="{{$consulta->id_direccion}}">{{$consulta->direccion}}</option>
                                @foreach($direcciones as $d)
                                <option value="{{$d->id_direccion}}">{{$d->direccion}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-10 pr-1">
                        <div class="form-group">
                            <label></label>
                            <button type="submit" class="btn btn-primary round">Guardar Actualización</button>
                                <button type="reset" class="btn btn-danger round" onclick="history.back()">Cancelar</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
@stop

@extends('principal')
@section('contenido')

<div class="col-md-12">
    <div class="card card-user">
        <div class="card-header">
            @if ($message = Session::get('success'))
            <div class="alert alert-success col-md-12" role="alert">{{ $message }}
            </div>
            @endif
            @if ($message = Session::get('warning'))
            <div class="alert alert-warning col-md-12" role="alert">{{ $message }}
            </div>
            @endif
            <h5 class="card-title">Modificación de Direcciones</h5>
        </div>
        <div class="card-body">
            <form action="{{route('actualiza_direccion')}}" method="POST">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-10 pr-1">
                    <div class="form-group">
                            <label>ID de la dirección:</label>
                            <input type="text" class="form-control form-control-sm" name="id_direccion" value="{{$consulta->id_direccion}}"
                                onKeyup="Upper(this);" readonly>
                        </div>
                        <div class="form-group">
                            <label>Nombre de la dirección:</label>
                            <input type="text" class="form-control form-control-sm" name="direccion" value="{{$consulta->direccion}}"
                                onKeyup="Upper(this);" required="">
                        </div>
                    </div>
                    <div class="col-md-10 pr-1">
                        <div class="form-group">
                            <label></label>
                            <button type="submit" class="btn btn-primary round">Guardar Dirección</button>
                            <button type="reset" class="btn btn-danger round" onclick="history.back()">Cancelar</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
@stop

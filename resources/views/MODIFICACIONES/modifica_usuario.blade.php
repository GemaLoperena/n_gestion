@extends('principal')
@section('contenido')

<div class="col-md-12">
    <div class="card card-user">
        <div class="card-header">
            @if ($message = Session::get('success'))
            <div class="alert alert-success col-md-12" role="alert">{{ $message }}
            </div>
            @endif
            @if ($message = Session::get('warning'))
            <div class="alert alert-warning col-md-12" role="alert">{{ $message }}
            </div>
            @endif
            <h5 class="card-title">Modificación de Usuario</h5>
        </div>
        <div class="card-body">
            <form action="{{route('actualiza_usuario')}}" method="POST">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-1 pr-1">
                        <div class="form-group">
                            <label>ID de usuario:</label>
                            <input type="text" class="form-control form-control-sm" name="id_usuario"
                                value="{{$consulta->id_usuario}}" readonly>
                        </div>
                    </div>
                    <div class="col-md-3 px-1">
                        <div class="form-group">
                            <label>Nombre(s):</label>
                            <input type="text" class="form-control form-control-sm" name="nom_usuario"
                                value="{{$consulta->nom_usuario}}" onKeyup="Upper(this);" required="">
                        </div>
                    </div>
                    <div class="col-md-4 px-1">
                        <div class="form-group">
                            <label>Apellido Paterno:</label>
                            <input type="text" class="form-control form-control-sm" name="app_usuario"
                                value="{{$consulta->app_usuario}}" onKeyup="Upper(this);" required="">
                        </div>
                    </div>
                    <div class="col-md-4 pl-1">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Apellido Materno:</label>
                            <input type="text" class="form-control form-control-sm" name="apm_usuario"
                                value="{{$consulta->apm_usuario}}" onKeyup="Upper(this);" required="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 pr-1">
                        <div class="form-group">
                            <label>Profesión:</label>
                            <input type="text" class="form-control form-control-sm" name="profesion"
                                value="{{$consulta->profesion}}" onKeyup="Upper(this);" required="">
                        </div>
                    </div>
                    <div class="col-md-3 px-1">
                        <div class="form-group">
                            <label>Contraseña Actual:</label>
                            <input type="password" class="form-control form-control-sm"
                                value="{{$consulta->contrasena}}" onKeyup="Upper(this);" name="contrasena_anterior" required="">
                        </div>
                    </div>
                    <div class="col-md-3 px-1">
                        <div class="form-group">
                            <label>Cambio de Contraseña:</label>
                            <input type="password" class="form-control form-control-sm" id="contrasena"
                                onKeyup="Upper(this);">
                        </div>
                    </div>
                    <div class="col-md-3 pl-1">
                        <div class="form-group">
                            <label>Repetir Contraseña:</label>
                            <input type="password" class="form-control form-control-sm" name="contrasena1"
                                id="contrasena1" style="text-transform:uppercase" onKeyup="verificarPasswords()">
                        </div>
                    </div>
                    <div class="col-md-2 pr-1">
                        <div class="form-group">
                            <label>Estado:</label><br>
                            @if($consulta->activo == 1)
                            <div class="form-check-inline form-check">
                                <label>
                                    <input type="radio" id="radio1" name="activo" value="1" class="form-check-input"
                                        checked>Activo
                                </label>
                            </div>
                            <div class="form-check-inline form-check">
                                <label>
                                    <input type="radio" id="radio2" name="activo" value="0"
                                        class="form-check-input">Inactivo
                                </label>
                            </div>
                            @endif
                            @if($consulta->activo == 0)
                            <div class="form-check-inline form-check">
                                <label>
                                    <input type="radio" id="radio1" name="activo" value="1" class="form-check-input"
                                        >Activo
                                </label>
                            </div>
                            <div class="form-check-inline form-check">
                                <label>
                                    <input type="radio" id="radio2" name="activo" value="0"
                                        class="form-check-input" checked>Inactivo
                                </label>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3 pr-1">
                        <div class="form-group">
                            <label>Correo:</label>
                            <input type="text" class="form-control form-control-sm" value="{{$consulta->usuario}}"
                                name="usuario" required="">
                        </div>
                    </div>
                    <div class="col-md-4 px-1">
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Dirección:</label>
                            <select class="form-control form-control-sm" name="id_direccion" required="">
                                <option value="{{$consulta->id_direccion}}">{{$consulta->direccion}}</option>
                                @foreach($direcciones as $d)
                                <option value="{{$d->id_direccion}}">{{$d->direccion}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 pl-1">
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Tipo de usuario:</label>
                            <select class="form-control form-control-sm" name="id_rol" required="">
                                <option value="{{$consulta->id_rol}}">{{$consulta->rol}}</option>
                                @foreach($roles as $r)
                                <option value="{{$r->id_rol}}">{{$r->rol}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="typography-line" id="incorrecto" style="display: none;">
                        <p class="text-danger">Las contraseñas son diferentes, intente nuevamente</p>
                    </div>
                    <div class="typography-line" id="correcto" style="display: none;">
                        <p class="text-success">Las contraseñas coinciden, puede continuar</p>
                    </div>
                    <div class="update ml-auto mr-auto">
                        <button type="submit" class="btn btn-primary round" id="guardar">Actualiza Usuario</button>
                        <button type="reset" class="btn btn-danger round" onclick="history.back()">Cancelar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop

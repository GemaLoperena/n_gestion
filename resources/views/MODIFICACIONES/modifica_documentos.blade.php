@extends('principal')
@section('contenido')

<div class="col-md-12">
    <div class="card card-user">
        <div class="card-header">
            @if ($message = Session::get('success'))
            <div class="alert alert-success col-md-12" role="alert">{{ $message }}
            </div>
            @endif
            @if ($message = Session::get('warning'))
            <div class="alert alert-warning col-md-12" role="alert">{{ $message }}
            </div>
            @endif
            <h5 class="card-title">Modificación de Direcciones</h5>
        </div>
        <div class="card-body">
            <form action="{{route('actualiza_documento')}}" method="POST">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-6 pr-1">
                        <div class="form-group">
                            <label>ID del Documento:</label>
                            <input type="text" class="form-control form-control-sm" name="id_tdocumento"
                                value="{{$documentos->id_tdocumento}}" onKeyup="Upper(this);" readonly>
                        </div>
                    </div>
                    <div class="col-md-6 pr-1">
                        <div class="form-group">
                            <label>Nombre del Documento:</label>
                            <input type="text" class="form-control form-control-sm" name="descripcion"
                                value="{{$documentos->descripcion}}" onKeyup="Upper(this);" required="">
                        </div>
                    </div>
                    <div class="col-md-6 pr-1">
                        <label>Estado:</label>
                        <div class="form-check">
                            <label>Activo</label>
                            <input class="form-check-input" type="radio" value="1" name="activo" id="activo">
                        </div>
                        <div class="form-check">
                            <label>Inactivo</label>
                            <input class="form-check-input" type="radio" value="0" name="activo" id="activo" checked>
                        </div>
                    </div>
                    <div class="col-md-6 pr-1">
                        <div class="form-group">
                            <label></label>
                            <button type="submit" class="btn btn-primary round">Guardar Dirección</button>
                            <button type="reset" class="btn btn-danger round" onclick="history.back()">Cancelar</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
@stop

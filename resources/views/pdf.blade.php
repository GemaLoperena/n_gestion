<?php 


foreach($recepciones as $r)
    {
        $turnos = json_decode($r->turnado);
        $ccp = json_decode($r->ccp);
    }
    

?>

<header>
    <style>
        .fecha_lib {
            width: 70%;
            border: 0.0px solid white
        }

        .h1 {
            text-align: center;
            font-size: 12px;
            font-weight: bold;
            font-family: Arial, Helvetica, sans-serif;
        }

        .p_left {
            text-align: left;
            font-size: 8px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .p_vigencia {
            text-align: left;
            font-size: 10px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .p_ccp {
            text-align: left;
            font-size: 8px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .p_right {
            text-align: right;
            font-size: 10px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .p_center {
            text-align: center;
            font-size: 8px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .p_justify {
            text-align: justify;
            font-size: 8px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .table,
        td,
        tr {
            border-collapse: collapse;
            border: 0.5px solid black;
            vertical-align: middle;
        }

        .logo,
            {
            border-collapse: collapse;
            border: 0px;
            vertical-align: middle;
        }

    </style>
</header>
<table class="logo" CELLPADDING="6" width="100%">
    <img src="{{public_path('dist/assets/images/header1.png')}}" width="100%">
</table>
<p class="h1">SISTEMA DE CONTROL DE GESTIÓN<br>Oficialía de Partes del Fiscal General</p>
@foreach($recepciones as $r)
<p class="p_right">TOLUCA, ESTADO DE MEXICO<br>Turno número: {{$r->turno}}</p>
<table CELLPADDING="4">
    <tr>
        <th colspan="7">
            <p class="p_left">Documento que se turna: <br>{{$r->descripcion}}</p>
        </th>
        <th colspan="3"> </th>
    </tr>
</table>
<table class="table" CELLPADDING="6" width="100%">
    <tr>
        <td>
            <p class="p_center">Oficio no.</p>
        </td>
        <td>
            <p class="p_center">Asunto</p>
        </td>
        <td>
            <p class="p_center">Remitente</p>
        </td>
    </tr>
    <tr>
        <td>
            <p class="p_center">{{$r->no_oficio}}</p>
        </td>
        <td>
            <p class="p_justify">{{$r->asunto}}</p>
        </td>
        <td>
            <p class="p_center">{{$r->remitente}}</p>
        </td>
    </tr>
    @if($r->id_criterio == 1)
    <tr>

        <td colspan="3" style="background-color:#E8D70E">
            <p class="p_vigencia">Vigencia: <b> {{$r->vigencia}} </b></p>
        </td>
    </tr>
    @endif
    @if($r->id_criterio == 2)
    <tr>

        <td colspan="3" style="background-color:#E8880E">
            <p class="p_vigencia">Vigencia: <b> {{$r->vigencia}} </b></p>
        </td>
    </tr>
    @endif
    @if($r->id_criterio == 3)
    <tr>

        <td colspan="3" style="background-color:#FA5858">
            <p class="p_vigencia">Vigencia: <b> {{$r->vigencia}} </b></p>
        </td>
    </tr>
    @endif


</table><br>
@endforeach
<table class="table" CELLPADDING="11" width="100%">
    <thead>
        <tr>
            <th>
                <p class="p_left">Directivo:</p>
            </th>
            <th>
                <p class="p_left">Turnado a:</p>
            </th>

            <th>
                <p class="p_left">Directivo:</p>
            </th>
            <th>
                <p class="p_left">Con C.C.P. a:</p>
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($directivos as $d )
        @if(!empty($turnos))
        @foreach($turnos as $t)
        @if($t->id_direccion == $d->id_direccion)
        <tr>
            <td>
                <p class="p_left">{{$d->nombre}}</p>
            </td>
            <td>
                <p class="p_center">X</p>
            </td>

            <td>
                <p class="p_left"></p>
            </td>
            <td>
                <p class="p_center"></p>
            </td>
        </tr>
        @endif
        @endforeach
        @endif

        @if(!empty($ccp))
        @foreach($ccp as $c)
        @if($d->id_direccion == $c->id_direccion)
        <tr>
            <td>
                <p class="p_left"></p>
            </td>
            <td>
                <p class="p_center"></p>
            </td>
            <td>
                <p class="p_left">{{$d->nombre}}</p>
            </td>
            <td>
                <p class="p_center">X</p>
            </td>
        </tr>
        @endif
        @endforeach
        @endif

        @endforeach
    </tbody>
</table>
<br>
<table class="table" CELLPADDING="4" width="100%">
    <tr>
        <td colspan="16">
            <p class="p_left">Observaciones:</p>
        </td>
    </tr>
    <tr>
        @if(empty($recepciones->observaciones))
        <td colspan="16">
            <p class="p_justify">No hay Observaciones</p>
        </td>
        @endif
        @if(isset($recepciones->observaciones))
        <td colspan="16">
            <p class="p_justify"><?php echo $recepciones[0]->observaciones;?></p>
        </td>
        @endif
    </tr>
</table>
<br>

<p class="p_center"><b>ATENTAMENTE</b><br><br><br></p>
<p class="p_center">___________________________________</p>
<p class="p_center">{{Session::get('nombre')}} {{Session::get('app')}} {{Session::get('apm')}}</p>

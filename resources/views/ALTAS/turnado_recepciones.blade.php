@extends('principal')
@section('contenido')
<?php
    $turnos = json_decode($consulta->turnado);
    $ccp = json_decode($consulta->ccp);
?>
    <div class="col-md-12">
        @if ($message = Session::get('success'))
            <div class="alert alert-success col-md-12" role="alert">{{ $message }}
            </div>
        @endif
        @if ($message = Session::get('warning'))
            <div class="alert alert-warning col-md-12" role="alert">{{ $message }}
            </div>
        @endif

        <div class="card card-user">
            <div class="card-header">
                <h5 class="card-title">Actualización de Recepción</h5>
                <a  href="{{ route('download_pdf',['id' => $consulta->id_recepcion]) }}" target="_blank"><button type = "button" class="btn btn-success round" align="left">Descarga de Turno</button></a>
            </div>
            <div class="card-body">
                <form action="{{ route('turnar') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="row">
                        <div hidden>
                            <input type="text" class="form-control form-control-sm" name="id_recepcion"
                                value="{{ $consulta->id_recepcion }}" readonly="">
                            <input type="text" class="form-control form-control-sm" name="anio" value="{{ $consulta->anio }}"
                                readonly="">
                        </div>
                        <div class="col-md-3 pr-1">
                            <div class="form-group">
                                <label>Turno:</label>
                                <input type="text" class="form-control form-control-sm" name="turno"
                                    value="{{ $consulta->turno }}" readonly="">
                            </div>
                        </div>
                        <div class="col-md-2 px-1" hidden="">
                            <div class="form-group">
                                <label>Número de oficio:</label>
                                <input type="text" class="form-control form-control-sm" name="no_oficio"
                                    value="{{ $consulta->no_oficio }}" onKeyup="Upper(this);" readonly="">
                            </div>
                        </div>
                        <div class="col-md-2 px-1">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Fecha de oficio:</label>
                                <input type="text" class="form-control form-control-sm" name="fecha_oficio"
                                    value="{{ $consulta->fecha_oficio }}" readonly="">
                            </div>
                        </div>
                        <div class="col-md-2 px-1">
                            <div class="form-group">
                                <label>Fecha de recepción:</label>
                                <input type="text" class="form-control form-control-sm" name="fecha_recepcion"
                                    value="{{ $consulta->fecha_recepcion }}" readonly="">
                            </div>
                        </div>
                        <div class="col-md-3 pl-1" hidden="">
                            <div class="form-group">
                                <label>Hora:</label>
                                <input type="text" class="form-control form-control-sm" name="hora_recepcion"
                                    value="{{ $consulta->hora_recepcion }}" readonly="">
                            </div>
                        </div>
                        <div class="col-md-5 pr-1">
                            <div class="form-group">
                                <label for="remitente">Remitente:</label>
                                <input type="text" class="form-control form-control-sm" name="remitente"
                                    value="{{ $consulta->remitente }}" readonly="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 pr-1">
                            <div class="form-group form-label-group">
                                <label for="label-textarea">asunto</label>
                                <textarea class="form-control" id="label-textarea" name="asunto" rows="3"
                                    readonly="">{{ $consulta->asunto }}</textarea>
                            </div>
                        </div>
                        <div class="col-md-6 pr-1">
                            <div class="form-group form-label-group">
                                <label for="label-textarea">observaciones</label>
                                <textarea class="form-control" id="label-textarea" name="observaciones" rows="3"
                                    readonly="">{{ $consulta->observaciones }}</textarea>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 pr-1">
                            <div class="form-group">
                                <label>Estatus del turno:</label>
                                <input type="text" class="form-control form-control-sm" name="id_status"
                                    value="{{ $consulta->status }}" readonly="">
                            </div>
                        </div>

                        <div class="col-md-4 pl-1">
                            <div class="form-group">
                                <label>Registró:</label>
                                <input type="text" class="form-control form-control-sm" name="id_usuario"
                                    value="{{ $consulta->id_usuario }}" hidden>
                                <input type="text" class="form-control form-control-sm"
                                    value="{{ $consulta->nom_usuario }}" readonly="">
                            </div>
                        </div>
                         <!-- Multiple choices start -->
                        <div class="row">
                            <div class="col-md-6 mb-4">
                                <h6>Turnar a: </h6>
                                <div class="form-group">
                                    <select class="choices form-select select-light-danger" name="direccion[]" multiple="multiple">
                                        <optgroup label="Turnado">
                                            @foreach ($direcciones as $d )
                                                @if(!empty($turnos))
                                                    @foreach($turnos as $t)
                                                        @if($d->id_direccion == $t->id_direccion)
                                                            <option value="{{$d->id_direccion}}" selected>{{$d->direccion}}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                                <option value="{{$d->id_direccion}}">{{$d->direccion}}</option>
                                            @endforeach
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 mb-4">
                                <h6>Con copia a: </h6>
                                <div class="form-group">
                                    <select class="choices form-select select-light-danger" name="copia[]" multiple="multiple">
                                        <optgroup label="Con Copia">
                                            @foreach ($direcciones as $d )
                                                @if(!empty($ccp))
                                                    @foreach($ccp as $c)
                                                    @if($d->id_direccion == $c->id_direccion)
                                                            <option value="{{$d->id_direccion}}" selected>{{$d->direccion}}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                                <option value="{{$d->id_direccion}}">{{$d->direccion}}</option>
                                            @endforeach
                                        </optgroup>
                                    </select>
                                </div>

                            </div>
                        </div>
                        <!-- Multiple choices end -->
                    </div>
                    <div class="row">
                        <div class="update ml-auto mr-auto">
                            <button type="submit" class="btn btn-primary round" id="guardar">Actualiza Recepción</button>
                            <button type="reset" class="btn btn-danger round" onclick="history.back()">Cancelar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Include Choices JavaScript -->
    <script src="{{ asset('dist/assets/vendors/choices.js/choices.min.js') }}"></script>

@stop

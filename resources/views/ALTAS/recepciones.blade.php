@extends('principal')
@section('contenido')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="col-md-12">
        @if ($errors->any())
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endif

        @if ($message = Session::get('success'))
            <div class="alert alert-light-success color-success" role="alert">{{ $message }}
            </div>
        @endif
        @if ($message = Session::get('warning'))
            <div class="alert alert-light-warning color-success" role="alert">{{ $message }}
            </div>
        @endif
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Nueva Recepción</h5>
                <form action="{{ route('guarda_recepciones') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="row">
                        <input type="datetime" class="form-control form-control-sm" name="anio" value="{{ date('Y') }}" hidden>
                        <div class="col-md-2 pr-1">
                            <div class="form-group">
                                <label for="helperText">Nomenclatura</label>
                                <select name="nomenclatura" class="form-select" id="nomenclatura" required>
                                    <option value="">Nomenclatura</option>
                                    @foreach ($nomenclaturas as $nomenclatura)
                                        <option value="{{ $nomenclatura->id_nomenclatura }}">{{ $nomenclatura->nomenclatura }}- {{$nomenclatura->descripcion}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2 px-1">
                            <div class="form-group">
                                <label for="helperText">Turno:</label>
                                @foreach ($datos as $q)
                                    <input type="text" class="form-control" id="helperText" name="turno"
                                        value="{{ $q }}" readonly>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-md-2 px-1">
                            <div class="form-group">
                                <label for="helperText">Número de oficio:</label>
                                <input type="text" class="form-control" name="no_oficio"
                                    onKeyup="Upper(this);" required="" id="helperText" pattern="[A-Za-z0-9-/-]{5,40}" title="Se permiten únicamente letras, números y diagonales." >
                            </div>
                        </div>
                        <div class="col-md-2 pl-1">
                            <div class="form-group">
                                <label>Número de Expediente:</label>
                                <input type="num" class="form-control" name="expediente" id="expediente" readonly="">

                            </div>
                        </div>
                        <div class="col-md-2 px-1">
                            <div class="form-group">
                                <label for="helperText">Fecha de oficio:</label>
                                <input type="date" class="form-control"  id="helperText" name="fecha_oficio" required max="<?php $hoy=date("Y-m-d"); echo $hoy;?>">
                            </div>
                        </div>
                        <div class="col-md-2 px-1">
                            <div class="form-group">
                                <label>Fecha de recepción:</label>
                                <input type="date" class="form-control" name="fecha_recepcion" required max="<?php $hoy1=date("Y-m-d"); echo $hoy1;?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 pr-1">
                            <div class="form-group">
                                <label>Asunto:</label>
                                <textarea class="form-control form-control-lg" name="asunto" onKeyup="Upper(this);" required=""> </textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 pr-1">
                            <div class="form-group">
                                <label for="remitente">Remitente:</label>
                                <select name="remitente" class="form-select" id="basicSelect" required>
                                    <option value="">Remitente</option>
                                    @foreach ($remitentes as $remitente)
                                        <option value="{{ $remitente->id_remitente }}">{{ $remitente->remitente }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2 pr-1">
                            <div class="form-group">
                                <label for="id_criterio">Criterio:</label>
                                <select name="id_criterio" class="form-select" id="basicSelect" required>
                                    <option value="">Criterio</option>
                                    @foreach ($criterios as $c)
                                        <option value="{{ $c->id_criterio }}">{{ $c->criterio }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2 pr-1">
                            <div class="form-group">
                                <label for="id_criterio">Tipo de Documento:</label>
                                <select name="id_tdocumento" class="form-select" id="basicSelect" required>
                                    <option value="">Tipo de Documento</option>
                                    @foreach ($tdocumentos as $t)
                                        <option value="{{ $t->id_tdocumento }}">{{ $t->descripcion }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2 pr-1">
                            <div class="form-group">
                                <label>Registró:</label>
                                <input type="text" class="form-control" name="id_usuario"
                                    value="{{ Session::get('id_usuario') }}" hidden>
                                <input type="text" class="form-control"
                                    value="{{ Session::get('nombre') }}" readonly="">
                            </div>
                        </div>
                        <div class="col-md-2 pl-1">
                            <div class="form-group">
                                <label>Hora:</label>
                                <input type="datetime" class="form-control" name="hora_recepcion"
                                    value="{{ date('H:i:s') }}" readonly="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="update ml-auto mr-auto">
                            <button type="submit" class="btn btn-sm btn-primary" id="guardar">Guardar
                                Recepción</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
@stop

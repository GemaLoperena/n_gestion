<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FGJEM | Gestión</title>

    <link rel="shortcut icon" href="{{asset('dist/assets/images/favicon.ico')}}" type="image/x-icon">
    <!-- Include Choices CSS -->
    <link rel="stylesheet" href="{{asset('dist/assets/vendors/choices.js/choices.min.css')}}" />
    <link rel="stylesheet" href="{{asset('dist/assets/vendors/perfect-scrollbar/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('dist/assets/css/dataTables.bootstrap5.min.css')}}">
    <link rel="stylesheet" href="{{asset('dist/assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('dist/assets/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('dist/assets/css/app.css')}}">

    <script src="{{asset('dist/assets/js/jquery-3.3.1.js')}}"></script>
</head>

<body>
    <div id="app">
        <div id="sidebar" class='active'>
            <div class="sidebar-wrapper">
                <div class="sidebar-header">
                    <h2>GESTIÓN</h2>
                </div>
                <div class="sidebar-menu">
                    @if(Session::get('id_rol') == 1)
                    <ul class="menu">
                        <li class="sidebar-item">
                            <a href="{{route('recepciones')}}" class='sidebar-link'>
                                <i data-feather="home" width="20"></i>
                                <span>Recepciones</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="{{route('busqueda_recepcion')}}" class='sidebar-link'>
                                <i data-feather="search" width="20"></i>
                                <span>Busqueda de Turnos</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="{{route('busqueda_turnos')}}" class='sidebar-link'>
                                <i data-feather="file-text" width="20"></i>
                                <span>Respuestas a Turnado</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="{{route('busqueda_general')}}" class='sidebar-link'>
                                <i data-feather="search" width="20"></i>
                                <span>Búsqueda General</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="{{route('alta_usuarios')}}" class='sidebar-link'>
                                <i data-feather="user" width="20"></i>
                                <span>Usuarios</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="{{route('busqueda_usuarios')}}" class='sidebar-link'>
                                <i data-feather="search" width="20"></i>
                                <span>Búsqueda de usuarios</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="{{route('consulta_direcciones')}}" class='sidebar-link'>
                                <i data-feather="briefcase" width="20"></i>
                                <span>Direcciones</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="{{route('consulta_directivos')}}" class='sidebar-link'>
                                <i data-feather="home" width="20"></i>
                                <span>Directivos</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="{{route('consulta_remitentes')}}" class='sidebar-link'>
                                <i data-feather="briefcase" width="20"></i>
                                <span>Remitentes</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="{{route('consulta_documentos')}}" class='sidebar-link'>
                                <i data-feather="briefcase" width="20"></i>
                                <span>Documentos</span>
                            </a>
                        </li>
                    </ul>
                    @endif
                    @if(Session::get('id_rol') == 2)
                    <ul class="menu">
                        <li class="sidebar-item">
                            <a href="{{route('busqueda_recepcion')}}" class='sidebar-link'>
                                <i data-feather="search" width="20"></i>
                                <span>Respuestas a Turnado</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="index.html" class='sidebar-link'>
                                <i data-feather="search" width="20"></i>
                                <span>Búsqueda de oficios</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="index.html" class='sidebar-link'>
                                <i data-feather="search" width="20"></i>
                                <span>Búsqueda de circulares</span>
                            </a>
                        </li>
                    </ul>
                    @endif
                    @if(Session::get('id_rol') == 3)
                    <ul class="menu">
                        <li class="sidebar-item">
                            <a href="{{route('recepciones')}}" class='sidebar-link'>
                                <i data-feather="home" width="20"></i>
                                <span>Recepciones</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="{{route('busqueda_recepcion')}}" class='sidebar-link'>
                                <i data-feather="search" width="20"></i>
                                <span>Busqueda de Turnos</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="{{route('busqueda_turnos')}}" class='sidebar-link'>
                                <i data-feather="file-text" width="20"></i>
                                <span>Reporte de Turnos</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="#" class='sidebar-link'>
                                <i data-feather="file-text" width="20"></i>
                                <span>Consulta de turnos</span>
                            </a>
                        </li>
                    </ul>
                    @endif
                    @if(Session::get('id_rol') == 4)
                    <ul class="menu">
                        <li class="sidebar-item">
                            <a href="{{route('busqueda_turnos')}}" class='sidebar-link'>
                                <i data-feather="file-text" width="20"></i>
                                <span>Reporte de Turnos</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="{{route('busqueda_ccp')}}" class='sidebar-link'>
                                <i data-feather="check-circle" width="20"></i>
                                <span>C.C.P</span>
                            </a>
                        </li>
                    </ul>
                    @endif
                    @if(Session::get('id_rol') == 5)
                    <ul class="menu">
                        <li class="sidebar-item">
                            <a href="{{route('recepciones')}}" class='sidebar-link'>
                                <i data-feather="home" width="20"></i>
                                <span>Recepciones</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="{{route('busqueda_recepcion')}}" class='sidebar-link'>
                                <i data-feather="search" width="20"></i>
                                <span>Busqueda de Recepciones</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="{{route('busqueda_turnos')}}" class='sidebar-link'>
                                <i data-feather="file-text" width="20"></i>
                                <span>Reporte de Turnos</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="{{route('busqueda_general')}}" class='sidebar-link'>
                                <i data-feather="search" width="20"></i>
                                <span>Búsqueda General</span>
                            </a>
                        </li>
                        {{-- <li class="sidebar-item">
                            <a href="index.html" class='sidebar-link'>
                                <i data-feather="file-text" width="20"></i>
                                <span>Registro de oficios</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="index.html" class='sidebar-link'>
                                <i data-feather="search" width="20"></i>
                                <span>Búsqueda de oficios</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="index.html" class='sidebar-link'>
                                <i data-feather="file-text" width="20"></i>
                                <span>Registro de circulares</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="index.html" class='sidebar-link'>
                                <i data-feather="search" width="20"></i>
                                <span>Búsqueda de circulares</span>
                            </a>
                        </li> --}}
                    </ul>
                    @endif
                    @if(Session::get('id_rol') == 6)
                    <ul class="menu">
                        <li class="sidebar-item">
                            <a href="{{route('busqueda_turnos')}}"" class='sidebar-link'>
                                <i data-feather="file-text" width="20"></i>
                                <span>Reporte de Turnos</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="index.html" class='sidebar-link'>
                                <i data-feather="search" width="20"></i>
                                <span>C.C.P</span>
                            </a>
                        </li>
                    </ul>
                    @endif
                </div>
                <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
            </div>
        </div>
        <div id="main">
            <nav class="navbar navbar-header navbar-expand navbar-light">
                <a class="sidebar-toggler" href="#"><span class="navbar-toggler-icon"></span></a>
                <button class="btn navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav d-flex align-items-center navbar-light ms-auto">
                        <li class="dropdown nav-icon me-2">
                            <a href="#" data-bs-toggle="dropdown"
                                class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                                <div class="d-lg-inline-block">
                                    <i data-feather="user"></i>
                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-end">
                                <a class="dropdown-item" href="#"><i data-feather="user"></i> Account</a>
                                <a class="dropdown-item active" href="#"><i data-feather="mail"></i> Messages</a>
                                <a class="dropdown-item" href="#"><i data-feather="settings"></i> Settings</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{route('cerrar_sesion')}}"><i data-feather="log-out"></i>Cierre de sesión</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>

            <div class="main-content container-fluid">
                @yield('contenido')
            </div>

            <footer>
                <div class="footer clearfix mb-0 text-muted">
                    <div class="float-end">
                        <p>Dirección General de Tecnologías de la Información y Comunicaciones de la Fiscalía General de
                            Justicia Estado de México. Tel. 226 16 00 Ext. 3259 </p>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <script src="{{asset('dist/assets/js/feather-icons/feather.min.js')}}"></script>
    <script src="{{asset('dist/assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>


    <script src="{{asset('dist/assets/vendors/chartjs/Chart.min.js')}}"></script>
    <script src="{{asset('dist/assets/vendors/apexcharts/apexcharts.min.js')}}"></script>
    <script src="{{asset('dist/assets/js/pages/dashboard.js')}}"></script>

    <script src="{{asset('dist/assets/js/app.js')}}"></script>
    <script src="{{asset('dist/assets/js/main.js')}}"></script>
    <script src="{{asset('js/combo_nomen.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>
    <script src="{{asset('dist/tables/jquery-datatable.js')}}"></script>
    <script src="{{asset('dist/assets/vendors/chartjs/Chart.min.js')}}"></script>

    <script>
        function verificarPasswords() {
            var pass1 = $('#contrasena').val();
            var pass2 = $('#contrasena1').val();
            if (pass1 != pass2) {
                $('#incorrecto').show();
                $('#correcto').hide();
                $('#guardar').hide();
            } else {
                $('#incorrecto').hide();
                $('#correcto').show();
                $('#guardar').show();
            }
        }

    </script>
    <script>
        function Upper(e) {
            e.value = e.value.toUpperCase();
        }

    </script>
    <script>
        $('#alta').on('click', function () {

            $('#alta_direcciones').attr('hidden', false);
        });
        $('#cancelar').on('click', function () {

            $('#alta_direcciones').attr('hidden', true);
        });

    </script>
    <script>
        $('#alta_nuevo_directivo').on('click', function () {
            console.log("oshe");
            $('#alta_directivos').attr('hidden', false);
        });
        $('#cancelar_directivo').on('click', function () {
            console.log("oshe");
            $('#alta_directivos').attr('hidden', true);
        });

    </script>
    <script>
        $('#alta_remitente').on('click', function () {
            console.log("oshe");
            $('#alta_remitentes').attr('hidden', false);
        });
        $('#cancelar_remitente').on('click', function () {
            console.log("oshe");
            $('#alta_remitentes').attr('hidden', true);
        });

    </script>
        <script>
        $('#alta_documentos').on('click', function () {
            console.log("oshe");
            $('#alta_documento').attr('hidden', false);
        });
        $('#cancelar_documento').on('click', function () {
            console.log("oshe");
            $('#alta_documento').attr('hidden', true);
        });

    </script>
</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sistema de Control y Gestión</title>
    <link rel="stylesheet" href="{{asset('dist/assets/css/bootstrap.css')}}">
    <link rel="shortcut icon" href="{{asset('dist/assets/images/favicon.svg')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{asset('dist/assets/css/app.css')}}">
</head>

<body>
    <div id="auth">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-sm-12 mx-auto">
                    <div class="card pt-4">
                        <div class="card-body">
                            <div class="text-center mb-5">
                                <img src="{{asset('dist/assets/images/header1.png')}}" width="100%" class='mb-4'>
                                <h5>Sistema de Control y Gestión.</h5>
                                <p>Ingrese su usuario y contraseña para inicio de sesión.</p>
                            </div>
                            <form action="{{route('valida')}}" method="post">
                                {{csrf_field()}}
                                @if ($message = Session::get('danger'))
                                <div class="alert alert-light-danger color-danger">
                                    <strong>{{ $message }}</strong>
                                </div>
                                @endif
                                @if ($message = Session::get('warning'))
                                <div class="alert alert-light-warning color-danger">
                                    <strong>{{ $message }}</strong>
                                </div>
                                @endif
                                <div class="form-group position-relative">
                                    <label for="username">Usuario:</label>
                                    <div class="position-relative">
                                        <input type="text" class="form-control" name="usuario">
                                    </div>
                                </div>
                                <div class="form-group position-relative">
                                    <div class="clearfix">
                                        <label for="password">Contraseña:</label>
                                        <input type="password" class="form-control" name="contrasena">
                                        </a>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <button class="btn btn-primary float-end">Iniciar sesión</button>
                                </div>
                            </form>
                            <div class="row">
                                <div class="col-sm-12">
                                    <p>Dirección General de Tecnologías de la Información y Comunicaciones de la
                                        Fiscalía General de Justicia Estado de México. Tel. 226 16 00 Ext. 3259 </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script src="{{asset('dist/assets/js/feather-icons/feather.min.js')}}"></script>
    <script src="{{asset('dist/assets/js/app.js')}}"></script>
    <script src="{{asset('dist/assets/js/main.js')}}"></script>
</body>

</html>

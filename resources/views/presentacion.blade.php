@extends('principal')
@section('contenido')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Bienvenido al Sistema de Control y Gestión</h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <p class="form-control-static" id="staticInput">{{Session::get('nombre')}} {{Session::get('app')}} {{Session::get('apm')}}</p>
                    <p class="form-control-static" id="staticInput">{{$datos->profesion}}</p>
                    <p class="form-control-static" id="staticInput">{{$datos->direccion}}</p>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
    
@php 
    $turnado = json_decode($turnado, true);
    $direcciones = session('direcciones');
@endphp
@foreach ($direcciones as $d )
@if(!empty($turnado))
    @foreach($turnado as $t)
        @if($d->id_direccion == $t['id_direccion'])
            <span class="badge rounded-pill bg-primary" style="font-size: 10px">{{$d->direccion}}</span><br>
        @endif
    @endforeach
@endif
@endforeach


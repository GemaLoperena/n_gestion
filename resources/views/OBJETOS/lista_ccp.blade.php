
@php
    $ccp = json_decode($ccp, true);
    $direcciones = session('direcciones');
@endphp
@foreach ($direcciones as $d )
@if(!empty($ccp))
    @foreach($ccp as $c)
        @if($d->id_direccion == $c['id_direccion'])
        <span class="badge rounded-pill bg-primary" style="font-size: 10px">{{$d->direccion}}</span><br>
        @endif
    @endforeach
@endif
@endforeach

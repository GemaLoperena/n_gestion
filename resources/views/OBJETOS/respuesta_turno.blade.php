@extends('principal')
@section('contenido')
@if ($message = Session::get('success'))
<div class="alert alert-success col-md-12" role="alert">{{ $message }}
</div>
@endif
@if ($message = Session::get('warning'))
<div class="alert alert-warning col-md-12" role="alert">{{ $message }}
</div>
@endif
<div class="col-md-12">
    <div class="card card-user">
        <div class="card-body">
        <h5 class="card-title">Respuestas a Turnado</h5>
            <form action="{{route('guarda_respuesta_turnado')}}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-4 pr-1">
                        <div class="form-group">
                            <input type="hidden" name="id_recepcion" id="id_recepcion" value="{{$consulta[0]->id_recepcion}}"
                            <label>Turno:</label>
                            <input type="text" class="form-control" name="id_recepcion"
                                value="{{$consulta[0]->turno}}" readonly="">
                        </div>
                    </div>
                    <div class="col-md-4 px-1">
                        <div class="form-group">
                            <label>Fecha de recepción</label>
                            <input type="date" class="form-control" name="fecha_recepcion"
                                value="{{$consulta[0]->fecha_recepcion}}" readonly="">
                        </div>
                    </div>
                    <div class="col-md-4 pr-1">
                        <div class="form-group">
                            <label for="remitente">Remitente:</label>
                                <select name="remitente" class="form-select" id="basicSelect" disabled>
                                    <option value="">Remitente</option>
                                    @foreach ($remitentes as $remitente)
                                        @if($remitente->id_remitente == $consulta[0]->id_remitente)
                                            <option value="{{ $remitente->id_remitente }}" selected>{{ $remitente->remitente }}</option>
                                        @else
                                            <option value="{{ $remitente->id_remitente }}">{{ $remitente->remitente }}</option>
                                        @endif
                                    @endforeach
                                </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 pr-1">
                        <div class="form-group form-label-group">
                            <label for="label-textarea">Asunto:</label>
                            <textarea class="form-control" id="label-textarea" onKeyup="Upper(this);" name="asunto"
                                rows="3" readonly="">{{$consulta[0]->asunto}}</textarea>
                            <label for="label-textarea"></label>
                        </div>
                    </div>

                    <div class="col-md-6 pr-1">
                        <div class="form-group">
                            <label>Estatus del turno</label>
                            <select class="form-select" id="basicSelect" name="id_status"disabled>
                                @foreach($status as $s)
                                    @if($consulta[0]->id_status == $s->id_status)
                                        <option value="{{$s->id_status}}" selected>{{$s->status}}</option>
                                    @else
                                        <option value="{{$s->id_status}}">{{$s->status}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-1">
                        <div class="form-group form-label-group">
                            <label for="label-textarea">Observaciones:</label>
                            <textarea class="form-control" id="label-textarea" name="observaciones"
                                rows="3" readonly="">{{$consulta[0]->observaciones}}</textarea>
                        </div>
                    </div>

                    <div class="col-md-4 px-1">
                        <div class="form-group">
                            <label>Oficio petición:</label><br>
                            @if ($consulta[0]->archivo)
                                <a href="{{ route('download',['archivo' => $consulta[0]->archivo]) }}"><i class="feather-52" data-feather="file-text" width="500"></i></a>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-4 px-1">
                        <div class="form-group">
                            <label>Vigencia:</label>
                            <input type="text" class="form-control" name="no_oficio"
                                value="{{$consulta[0]->vigencia}}" onKeyup="Upper(this);" readonly="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 px-1">
                        <label class="badge bg-primary">Comentario de Turno</label>
                        <table class="table table-light">
                            <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Hora</th>
                                <th>Reporte</th>
                                <th>Archivo</th>
                                <th>Registró</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach ($respuestas as $respuesta)
                                    @if ($respuesta->tipo_reporte==0)
                                    @php

                                    $found_key = array_search($respuesta->id_usuario, array_column($usuarios, 'id_usuario'));

                                    @endphp
                                        <tr>
                                            <td>{{$respuesta->rr_fecha}}</td>
                                            <td>{{$respuesta->rr_hora}}</td>
                                            <td>{{$respuesta->rr_descripcion}}</td>
                                            <td>@if($respuesta->rr_archivo)
                                                <a  href="{{ route('descarga_respuesta',['archivo' => $respuesta->rr_archivo]) }}"><i data-feather="download" width="30" ></i></a>
                                                @endif
                                            </td>
                                            <td>{{$usuarios[$found_key]->nom_usuario}}</td>
                                                <td><a href="{{route('eliminar_respuesta', $respuesta->id_reporte_recepciones)}}" @if($prueba != 0)  class="btn btn-danger" @else class="btn disabled btn-danger" @endif>Eliminar</a></td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 px-1">
                        <label class="badge bg-primary">Respuestas a turnado</label>
                        <table class="table table-light">
                            <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Hora</th>
                                <th>Reporte</th>
                                <th>Archivo</th>
                                <th>Registró</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>

                                @foreach ($respuestas as $respuesta)
                                    @if ($respuesta->tipo_reporte==1)
                                    @php

                                            $found_key = array_search($respuesta->id_usuario, array_column($usuarios, 'id_usuario'));

                                    @endphp
                                        <tr>
                                            <td>{{$respuesta->rr_fecha}}</td>
                                            <td>{{$respuesta->rr_hora}}</td>
                                            <td>{{$respuesta->rr_descripcion}}</td>
                                            <td>@if($respuesta->rr_archivo)
                                                <a  href="{{ route('descarga_respuesta',['archivo' => $respuesta->rr_archivo]) }}"><i data-feather="download" width="30" ></i></a>
                                                @endif
                                            </td>
                                            <td>{{$usuarios[$found_key]->nom_usuario}}</td>
                                            <td><a href="{{route('eliminar_respuesta', $respuesta->id_reporte_recepciones)}}"  @if($prueba != 0)  class="btn btn-danger" @else class="btn disabled btn-danger" @endif  >Eliminar</a></td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @yield('respuestas')
            </form>

        </div>
    </div>
</div>

<style>
    .feather-16{
        width: 16px;
        height: 16px;
    }
    .feather-24{
        width: 24px;
        height: 24px;
    }
    .feather-32{
        width: 32px;
        height: 32px;
    }
    .feather-52{
        width: 52px;
        height: 52px;
    }
    </style>
    @stop



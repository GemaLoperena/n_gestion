<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RecepcionesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nomenclatura' => 'required',
            'turno' => 'required',
            'no_oficio'=> 'required',
            'fecha_oficio' => 'required|date',
            'fecha_recepcion' => 'required|date',
            'hora_recepcion' => 'required',
            'asunto' => 'required',
            'remitente' => 'required',
            'id_criterio' => 'required',
            'id_tdocumento' => 'required',
            'expediente' => 'required',
        ];
    }

    public function messages()
    {
        return[
            'nomenclatura.required'  => 'Debe seleccionar una :attribute.',
            'turno.required'  => 'El :attribute es obligatorio.',
            'no_oficio.required'  => 'El :attribute es obligatorio.',
            'fecha_oficio.required'  => 'La :attribute es obligatoria.',
            'fecha_oficio.date'  => 'La :attribute debe ser una fecha valida.',
            'fecha_recepcion.required'  => 'La :attribute es obligatoria.',
            'fecha_recepcion.date'  => 'La :attribute debe ser una fecha valida..',
            'hora_recepcion.required'  => 'La :attribute debe contener mas de una letra.',
            'asunto.required'  => 'El :attribute debe contener mas de una letra.',
            'id_criterio.required'  => 'Debe seleccionar una opción de :attribute.',
            'id_tdocumento.required'  => 'Debe seleccionar una opción de :attribute.',
            'expediente.required'  => 'El :attribute es obligatorio.',
            ];
    }

    public function attributes()
    {
        return [
            'nomenclatura' => 'Nomenclatura',
            'turno' => 'Turno del oficio',
            'no_oficio' => 'Numero de oficio',
            'fecha_oficio' => 'Fecha de oficio',
            'fecha_recepcion' => 'Fecha de recepción',
            'hora_recepcion' => 'Hora de recepción',
            'asunto' => 'Asunto',
            'remitente' => 'Remitente',
            'id_criterio' => 'Criterio',
            'id_tdocumento' => 'Tipo de Documento',
            'expediente' => 'No. de Expediente'

        ];
    }
}


<?php

namespace App\Http\Controllers;

use App\Http\Requests\RecepcionesRequest;
use App\Models\Nomenclaturas;
use App\Models\Recepciones;
use App\Models\Remitentes;
use App\Models\Tipodocumentos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use PDF;
use Storage;
use Yajra\DataTables\DataTables;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use Excel;
use App\Exports\ReporteExport;

class RecepcionesController extends Controller
{

    public function is_loggin()
    {
        $is_loggin = session('is_loggin');

        if ($is_loggin == "" and $is_loggin == null) {

            Session::flash('danger', 'Iniciar sesión');

            return redirect()->route('login')->send();

        }
    }

    public function generaConsecutivo($start, $count, $digits)
    {
        $this->is_loggin();

        $result = array();
        for ($n = $start; $n < $start + $count; $n++) {
            $result[] = $n;
        }
        return $result;
    }

    public function recepciones()
    {
        $this->is_loggin();

        $actual = date("Y");
        $criterios = DB::table('criterios')->get();
        $col = Recepciones::where('anio', '=', $actual)->orderBy('turno', 'desc')->limit(1)->get();
        $remitentes = Remitentes::where('activo', '=', 1)->orderBy('remitente', 'asc')->get();
        $nomenclaturas = Nomenclaturas::where('activo', '=', 1)->orderBy('nomenclatura', 'asc')->get();
        $tdocumentos = Tipodocumentos::where('activo', '=', 1)->orderBy('descripcion', 'asc')->get();

        if ($col->count() == 0) {
            $consecutivo = $this->generaConsecutivo(1, 1, 4);
            $datos['n'] = $consecutivo[0];
        } else {
            $nomen = $col[0]->turno;
            $consecutivo = $this->generaConsecutivo($nomen + 1, 1, 4); //llama la función para sumar 1
            $datos['n'] = $consecutivo[0];
        }

        return view('ALTAS.recepciones')
            ->with('col', $col)
            ->with('datos', $datos)
            ->with('remitentes', $remitentes)
            ->with('nomenclaturas', $nomenclaturas)
            ->with('criterios', $criterios)
            ->with('tdocumentos', $tdocumentos);

    }

    public function guarda_recepciones(RecepcionesRequest $request)
    {

        $this->is_loggin();

        $no_oficio = $request->get('no_oficio');
        $count = Recepciones::where('no_oficio','=',$no_oficio)->count();

        if($count > 0)
        {
            Session::flash('warning', 'Ya se ha registrado el No de Oficio.');
            return redirect('recepciones');
        }


        $recep = new Recepciones;
        $recep->turno = $request->get('turno');
        $recep->id_nomenclatura = $request->get('nomenclatura');
        $recep->no_expediente = $request->get('expediente');
        $recep->no_oficio = $no_oficio;
        $recep->fecha_oficio = $request->get('fecha_oficio');
        $recep->fecha_recepcion = $request->get('fecha_recepcion');
        $recep->hora_recepcion = $request->get('hora_recepcion');
        $recep->asunto = $request->get('asunto');
        $recep->id_remitente = $request->get('remitente');
        $recep->anio = $request->get('anio');
        $recep->id_usuario = $request->get('id_usuario');
        $recep->id_status = 1;
        $recep->situacion = 1;
        $recep->id_criterio = $request->get('id_criterio');
        $recep->vigencia = null;
        $recep->procedencia = null;
        $recep->observaciones = null;
        $recep->prioritario = null;
        $recep->r_conjunta = 0;
        $recep->asignado = 0;
        $recep->activo = 1;
        $recep->archivo = null;
        $recep->id_tdocumento = $request->get('id_tdocumento');
        $recep->save();
        Session::flash('success', 'La recepción se ha dado de alta correctamente.');
        return redirect('recepciones');
    }

    public function busqueda_recepcion()
    {
        $this->is_loggin();

        return view('BUSQUEDAS.busqueda_recepciones');

    }

    public function recepcion_lista_json()
    {
        $this->is_loggin();

        $query = DB::table('recepciones')
            ->select('usuarios.id_usuario as id_usuario',
                'usuarios.nom_usuario as nom_usuario',
                'usuarios.activo as activo',
                'recepciones.id_recepcion as id_recepcion',
                'recepciones.turno as turno',
                'recepciones.no_oficio as no_oficio',
                'recepciones.fecha_oficio as fecha_oficio',
                'recepciones.fecha_recepcion as fecha_recepcion',
                'recepciones.hora_recepcion as hora_recepcion',
                'recepciones.asunto as asunto',
                'recepciones.anio as anio',
                'criterios.criterio as criterio',
                'remitentes.remitente as remitente')
            ->join('usuarios', 'recepciones.id_usuario', '=', 'usuarios.id_usuario')
            ->join('criterios', 'recepciones.id_criterio', '=', 'criterios.id_criterio')
            ->join('remitentes', 'recepciones.id_remitente', '=', 'remitentes.id_remitente');

        $jsonData = DataTables::of($query)
        // ->addColumn('Editar','<a href="{{route(\'editar\',$id_publicacion)}}" class="btn btn-warning">'.('Editar').'</a>')
        // ->addColumn('Eliminar','<a href="{{route(\'eliminar\',$id_publicacion)}}" class="btn btn-danger">'.('Eliminar').'</a>')
        // ->rawColumns(['Editar','Eliminar'])
            ->addColumn('btn_c', 'BOTONES.botones_criticidad')
            ->addColumn('btn', 'BOTONES.botones_recepciones')
            ->rawColumns(['btn', 'btn_c'])
            ->toJson();

        return $jsonData;
    }

    public function muestra_resultados_recepciones()
    {
        return view('RESULTADOS.resultados_recepciones');
    }

    public function modifica_recepciones($id)
    {
        $this->is_loggin();

        $status = DB::SELECT("SELECT * FROM status");
        $remitentes = Remitentes::where('activo', '=', 1)->orderBy('remitente', 'desc')->get();
        $criterios = DB::table('criterios')->get();
        $nomenclaturas = DB::table('nomenclaturas')->get();

        $consulta = DB::select("SELECT r.id_recepcion, r.turno, r.procedencia, r.id_nomenclatura, n.nomenclatura, r.no_expediente, n.descripcion, r.observaciones,r.archivo, r.no_oficio, r.fecha_oficio, r.fecha_recepcion,
                                        r.hora_recepcion,r.anio, r.asunto, r.id_remitente, u.id_usuario, u.nom_usuario, u.app_usuario,
                                        r.vigencia,r.r_conjunta, r.prioritario, r.situacion,u.apm_usuario, s.id_status, s.status, c.id_criterio
                                        from recepciones AS r, usuarios AS u, criterios as c,
                                        status AS s, nomenclaturas as n WHERE r.id_usuario = u.id_usuario  AND r.id_status = s.id_status AND r.id_criterio = c.id_criterio AND r.id_nomenclatura = n.id_nomenclatura
                                        AND r.id_recepcion='$id'");
        return view('MODIFICACIONES.modifica_recepciones')->with('consulta', $consulta[0])
            ->with('status', $status)
            ->with('remitentes', $remitentes)
            ->with('criterios', $criterios)
            ->with('nomenclaturas', $nomenclaturas);

    }

    public function guarda_actualizacion(Request $request)
    {

        $this->is_loggin();

        $nombre_archivo = "acuse_" . date("Hsi") . date("dmY");
        $obs = $request->get('observaciones');
        $prior = $request->get('prioritario');
        $recep = Recepciones::find($request->id_recepcion);
        $recep->id_recepcion = $request->get('id_recepcion');
        $recep->turno = $request->get('turno');
        $recep->no_oficio = $request->get('no_oficio');
        $recep->fecha_oficio = $request->get('fecha_oficio');
        $recep->fecha_recepcion = $request->get('fecha_recepcion');
        $recep->hora_recepcion = $request->get('hora_recepcion');
        $recep->asunto = $request->get('asunto');
        $recep->id_remitente = $request->get('remitente');
        $recep->anio = $request->get('anio');
        $recep->id_usuario = $request->get('id_usuario');
        $recep->id_status = $request->get('id_status');
        $recep->situacion = $request->get('situacion');
        $recep->prioritario = $request->get('atencion');
        $recep->id_criterio = $request->get('id_criterio');
        $recep->vigencia = $request->get('vigencia');
        $recep->procedencia = $request->get('procedencia');
        $recep->r_conjunta = $request->get('r_conjunta');
        if (isset($obs)) {
            $recep->observaciones = $obs;
        } else {
            $recep->observaciones = null;
        }
        $file = $request->file('archivo');
        if (isset($file)) {
            $extension = $file->getClientOriginalExtension();
            $path = $request->file('archivo')->storeAs('oficios', $nombre_archivo . "." . $extension);
            $string = Str::of("$path")->ltrim('oficios/');
            $recep->archivo = $string;
            if (Storage::disk('oficios')->exists($file)) {
                Session::flash('success', 'error.');
                return redirect()->back();
            }
        } else {
            $recep->archivo = $request->get('archivo_n');
        }

        $recep->save();
        Session::flash('success', 'La recepción se ha modificado correctamente.');
        return redirect()->back();
    }

    public function busqueda_general()
    {
        $this->is_loggin();

        return view('BUSQUEDAS.busqueda_general');

    }

    public function descargas($archivo)
    {

        return Storage::disk('oficios')->download("$archivo");
    }

    public function get_expediente(Request $request)
    {
        $this->is_loggin();

        $id_nomenclatura = $request->get('id_nomenclatura');
        $expediente = $this->generaExpediente($id_nomenclatura);
        return $expediente;
        //dd($expediente);

    }

    public function generaExpediente($id_nomenclatura)
    {

        $this->is_loggin();
        $anio = date("Y");

        $x = Recepciones::where('anio', '=', $anio)
            ->where('id_nomenclatura', '=', $id_nomenclatura)
            ->orderBy('no_expediente', 'desc')->limit(1)->get();

        if ($x->count() == 0) {
            $no_expediente = 1;
        } else {
            $expediente = $x[0]['no_expediente'];
            $no_expediente = $expediente + 1;
        }

        return $no_expediente;

    }

    public function general_lista_json()
    {
        $this->is_loggin();

        $direcciones = DB::SELECT("SELECT * FROM direcciones");
        session(['direcciones' => $direcciones]);

        $query = DB::table('recepciones')
        ->select(
            'recepciones.turno',
            'recepciones.id_recepcion as id_recepcion',
            'recepciones.no_oficio',
            'recepciones.fecha_oficio',
            'recepciones.fecha_recepcion',
            'recepciones.anio',
            'recepciones.asunto',
            'remitentes.remitente',
            'turnados.turnado as turnado',
            'turnados.ccp as ccp',
            'recepciones.observaciones',
            DB::raw('(CASE recepciones.prioritario
                WHEN  1 THEN "PRIORITARIO"
                WHEN  0 THEN "ORDINARIO"
                ELSE "" END) AS prioritario'),
            'recepciones.vigencia',
            'status.status',
            )
            ->join('usuarios', 'recepciones.id_usuario', '=', 'usuarios.id_usuario')
            ->join('status', 'recepciones.id_status', '=', 'status.id_status')
            ->join('criterios', 'recepciones.id_criterio', '=', 'criterios.id_criterio')
            ->join('remitentes', 'recepciones.id_remitente', '=', 'remitentes.id_remitente')
            ->join('turnados', 'turnados.id_recepcion', '=', 'recepciones.id_recepcion');

        $jsonData = DataTables::of($query)
        // ->addColumn('Editar','<a href="{{route(\'editar\',$id_publicacion)}}" class="btn btn-warning">'.('Editar').'</a>')
        // ->addColumn('Eliminar','<a href="{{route(\'eliminar\',$id_publicacion)}}" class="btn btn-danger">'.('Eliminar').'</a>')
        // ->rawColumns(['Editar','Eliminar'])
        ->addColumn('ccp','OBJETOS.lista_ccp')
        ->addColumn('turnado','OBJETOS.lista_turnados')
        ->addColumn('btn', 'BOTONES.botones_general')
        ->rawColumns(['btn', 'ccp','turnado'])
            ->toJson();
        return $jsonData;

    }
    public function download_pdf($id)
    {
        $directivos = DB::select("SELECT * FROM directivos");
        $direcciones = DB::select("SELECT * FROM direcciones");
        $recepciones = Recepciones::Join('usuarios', 'usuarios.id_usuario', '=', 'recepciones.id_usuario')
            ->Join('status', 'status.id_status', '=', 'recepciones.id_status')
            ->LeftJoin('remitentes', 'remitentes.id_remitente', '=', 'recepciones.id_remitente')
            ->LeftJoin('tipodocumentos', 'tipodocumentos.id_tdocumento', '=', 'recepciones.id_tdocumento')
            ->join('turnados', 'turnados.id_recepcion', '=', 'recepciones.id_recepcion')
            ->where('recepciones.id_recepcion', '=', $id)
            ->get(['recepciones.*', 'usuarios.id_usuario', 'usuarios.nom_usuario', 'usuarios.app_usuario', 'usuarios.apm_usuario',
                'status.id_status', 'status.status', 'turnados.turnado', 'turnados.ccp', 'remitentes.remitente', 'tipodocumentos.descripcion']);

        //    view()->share('users.pdf',$recepciones);

        $pdf = PDF::loadView('pdf', ['recepciones' => $recepciones, 'directivos' => $directivos, 'direcciones' => $direcciones]);

        return $pdf->stream('Turno_' . "$id" . '.pdf');

    }

    public function export()
    {
        return Excel::download(new ReporteExport, 'Reporte_General.xlsx');
    }

    public function muestra_general($id)
    {

        $this->is_loggin();

        $prueba = 0;
        $status = DB::SELECT("SELECT * FROM status");
        $remitentes = Remitentes::where('activo', '=', 1)->orderBy('remitente', 'desc')->get();
        $usuarios = DB::SELECT("SELECT nom_usuario, id_usuario FROM usuarios");

        $consulta = Recepciones::Join('usuarios','usuarios.id_usuario','=','recepciones.id_usuario')
        ->Join('status','status.id_status','=','recepciones.id_status')
        ->LeftJoin('turnados','turnados.id_recepcion', '=','recepciones.id_recepcion')
        ->where('recepciones.id_recepcion','=',$id)
        ->get(['recepciones.*',
               'usuarios.id_usuario',
               'usuarios.nom_usuario',
               'usuarios.app_usuario',
               'usuarios.apm_usuario',
               'status.id_status',
               'status.status',
               'turnados.turnado']);

               $respuestas = DB::table('reporte_recepciones')
                           ->where('id_recepcion','=',$id)
                           ->get();
        return view('FUNCIONES.respuesta_general_turno')->with('status',$status)
        ->with('remitentes',$remitentes)
        ->with('usuarios',$usuarios)
        ->with('consulta',$consulta)
        ->with('respuestas',$respuestas)
        ->with('prueba',$prueba);

    }
    public function eliminar_archivo ($archivo)
    {
        $deleted = DB::table('recepciones')
        ->where('archivo','=',$archivo)

        ->update(['archivo' => null]);
        Storage::disk('oficios')->delete("$archivo");
        return redirect(url()->previous());
    }

}

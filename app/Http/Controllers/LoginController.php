<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{

    public function login()
    {

        return view('welcome');
    }

    public function valida(Request $request)
    {
        $this->validate($request, [
            'usuario' => 'required',
            'contrasena' => 'required',
        ]);

        $usuarios = DB::table('usuarios')->where('usuario', '=', $request->usuario)->where('activo', '=', 1)->get();
        $cuantos = count($usuarios);
        $url_base = URL::to('/');

        foreach ($usuarios as $u)
        {
            if ($cuantos == 1 and hash::check($request->contrasena, $usuarios[0]->contrasena)) {
                Session::put('id_rol', $u->id_rol);
                Session::put('nombre', $u->nom_usuario);
                Session::put('app', $u->app_usuario);
                Session::put('apm', $u->apm_usuario);
                Session::put('id_usuario', $u->id_usuario);
                Session::put('id_direccion', $u->id_direccion);
                Session::put('is_loggin', true);
                return redirect('principal');
            }
        }
        Session::flash('danger', 'El usuario/password fueron incorrectos, intente nuevamente');
        return redirect('/');
    }
    public function principal()
    {
        $this->is_loggin();

        $id_usuario = session('id_usuario');
        $datos = DB::select("SELECT u.id_usuario, u.nom_usuario, u.app_usuario, u.apm_usuario, d.id_direccion, d.direccion, u.profesion FROM usuarios AS u, direcciones AS d WHERE u.id_direccion = d.id_direccion AND u.id_usuario = $id_usuario");

        return view('presentacion')->with('datos', $datos[0]);
    }

    public function is_loggin()
    {
        $is_loggin = session('is_loggin');

        if ($is_loggin =="" and $is_loggin == null)
        {

            Session::flash('danger', 'Iniciar sesión');

            return redirect()->route('login')->send();

        }
    }

    public function cerrar_sesion()
    {
        Session::put('is_loggin', false);
        Session::forget('id_usuario');
        Session::forget('is_loggin');
        Session::flush();
        Session::flash('warning', 'Sesión Cerrada Correctamente');
        return redirect('/');
    }


}

<?php

namespace App\Http\Controllers;

use App\Models\Direcciones;
use App\Models\Directivos;
use App\Models\Usuarios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;
use App\Models\Remitentes;
use App\Models\Tipodocumentos;

class AdministradorController extends Controller
{
    public function is_loggin()
    {
        $is_loggin = session('is_loggin');
        if ($is_loggin == "" and $is_loggin == null) {
            Session::flash('danger', 'Iniciar sesión');
            return redirect()->route('login')->send();
        }
    }

    public function alta_usuarios()
    {
        $this->is_loggin();
        $direcciones = DB::SELECT("SELECT * FROM direcciones");
        $roles = DB::SELECT("SELECT * FROM roles");
        return view('ADMINISTRADOR.alta_usuarios')->with('direcciones', $direcciones)->with('roles', $roles);
    }

    public function guarda_usuarios(Request $request)
    {
        $this->is_loggin();
        $usuarios = new Usuarios;
        $usuarios->nom_usuario = $request->get('nom_usuario');
        $usuarios->app_usuario = $request->get('app_usuario');
        $usuarios->apm_usuario = $request->get('apm_usuario');
        $usuarios->profesion = $request->get('profesion');
        $usuarios->activo = $request->get('activo');
        $usu = $usuarios->usuario = $request->get('usuario');
        $exist = DB::select("SELECT usuario from usuarios where usuario like '$usu'");
        $cuantos = count($exist);
        if ($cuantos == 0) {
            $usuarios->id_direccion = $request->get('id_direccion');
            $usuarios->id_rol = $request->get('id_rol');
            $usuarios->contrasena = Hash::make($request->get('contrasena1'));
            $usuarios->save();
        } else {
            Session::flash('warning', 'El usuario ya sido dado de alta anteriormente.');
            return redirect('alta_usuarios');
        }
        Session::flash('success', 'El usuario ha sido dado de alta.');
        return redirect('alta_usuarios');
    }

    public function busqueda_usuarios()
    {
        $this->is_loggin();
        $direcciones = DB::SELECT("SELECT * FROM direcciones");
        return view('ADMINISTRADOR.busqueda_usuarios')->with('direcciones', $direcciones);
    }

    public function direcciones_lista_json()
    {
        $this->is_loggin();
        $query = DB::table('direcciones');
        $jsonData = DataTables::of($query)
        // ->addColumn('Editar','<a href="{{route(\'editar\',$id_publicacion)}}" class="btn btn-warning">'.('Editar').'</a>')
        // ->addColumn('Eliminar','<a href="{{route(\'eliminar\',$id_publicacion)}}" class="btn btn-danger">'.('Eliminar').'</a>')
        // ->rawColumns(['Editar','Eliminar'])
            ->addColumn('btn', 'BOTONES.botones_direcciones')
            ->rawColumns(['btn'])
            ->toJson();
        return $jsonData;
    }

    public function usuarios_lista_json()
    {
        $this->is_loggin();
        $query = DB::table('usuarios')
            ->select('usuarios.id_usuario as id_usuario',
                'usuarios.nom_usuario as nom_usuario',
                'usuarios.activo as activo',
                'direcciones.direccion as direccion',
                'roles.rol as rol')
            ->join('direcciones', 'usuarios.id_direccion', '=', 'direcciones.id_direccion')
            ->join('roles', 'usuarios.id_rol', '=', 'roles.id_rol');
        $jsonData = DataTables::of($query)
        // ->addColumn('Editar','<a href="{{route(\'editar\',$id_publicacion)}}" class="btn btn-warning">'.('Editar').'</a>')
        // ->addColumn('Eliminar','<a href="{{route(\'eliminar\',$id_publicacion)}}" class="btn btn-danger">'.('Eliminar').'</a>')
        // ->rawColumns(['Editar','Eliminar'])
            ->addColumn('btn', 'BOTONES.botones_usuarios')
            ->rawColumns(['btn'])
            ->toJson();
        return $jsonData;
    }

    public function directivos_lista_json()
    {
        $this->is_loggin();
        $query = DB::table('directivos')
            ->select('directivos.id_directivo',
                'directivos.nombre',
                'directivos.activo',
                'direcciones.id_direccion',
                'direcciones.direccion')
            ->join('direcciones', 'directivos.id_direccion', '=', 'direcciones.id_direccion');
        $jsonData = DataTables::of($query)
        // ->addColumn('Editar','<a href="{{route(\'editar\',$id_publicacion)}}" class="btn btn-warning">'.('Editar').'</a>')
        // ->addColumn('Eliminar','<a href="{{route(\'eliminar\',$id_publicacion)}}" class="btn btn-danger">'.('Eliminar').'</a>')
        // ->rawColumns(['Editar','Eliminar'])
            ->addColumn('btn', 'BOTONES.botones_directivos')
            ->rawColumns(['btn'])
            ->toJson();
        return $jsonData;
    }

    public function remitentes_lista_json()
    {
        $this->is_loggin();
        $query = DB::table('remitentes');
        $jsonData = DataTables::of($query)
        // ->addColumn('Editar','<a href="{{route(\'editar\',$id_publicacion)}}" class="btn btn-warning">'.('Editar').'</a>')
        // ->addColumn('Eliminar','<a href="{{route(\'eliminar\',$id_publicacion)}}" class="btn btn-danger">'.('Eliminar').'</a>')
        // ->rawColumns(['Editar','Eliminar'])
            ->addColumn('btn', 'BOTONES.botones_remitentes')
            ->rawColumns(['btn'])
            ->toJson();
        return $jsonData;
    }
    public function documentos_lista_json()
    {
        $this->is_loggin();
        $query = DB::table('tipodocumentos');
        $jsonData = DataTables::of($query)
        // ->addColumn('Editar','<a href="{{route(\'editar\',$id_publicacion)}}" class="btn btn-warning">'.('Editar').'</a>')
        // ->addColumn('Eliminar','<a href="{{route(\'eliminar\',$id_publicacion)}}" class="btn btn-danger">'.('Eliminar').'</a>')
        // ->rawColumns(['Editar','Eliminar'])
            ->addColumn('btn', 'BOTONES.botones_documentos')
            ->rawColumns(['btn'])
            ->toJson();
        return $jsonData;
    }

    public function modifica_usuarios($id_usuario)
    {
        $this->is_loggin();
        $consulta = DB::select("SELECT u.id_usuario, u.nom_usuario, u.app_usuario, u.apm_usuario, u.usuario, u.profesion, u.contrasena, d.id_direccion, u.activo, d.direccion, r.id_rol, r.rol FROM usuarios AS u, 
                                direcciones AS d, roles AS r WHERE u.id_direccion=d.id_direccion AND u.id_rol=r.id_rol AND u.id_usuario = $id_usuario");
        $direcciones = DB::select("SELECT * from direcciones");
        $roles = DB::select("SELECT * FROM roles");
        return view('MODIFICACIONES.modifica_usuario')->with('consulta', $consulta[0])->with('direcciones', $direcciones)->with('roles', $roles);
    }

    public function actualiza_usuario(Request $request)
    {
        $this->is_loggin();
        $usuarios = Usuarios::find($request->id_usuario);
        $usuarios->id_usuario = $request->get('id_usuario');
        $usuarios->nom_usuario = $request->get('nom_usuario');
        $usuarios->app_usuario = $request->get('app_usuario');
        $usuarios->apm_usuario = $request->get('apm_usuario');
        $usuarios->profesion = $request->get('profesion');
        $usuarios->activo = $request->get('activo');
        $usu = $usuarios->usuario = $request->get('usuario');
        $usuarios->id_direccion = $request->get('id_direccion');
        $usuarios->id_rol = $request->get('id_rol');
        $contrasena = $request->get('contrasena1');
        $con = $request->get('contrasena_anterior');
        if (!empty($contrasena)) {
            $con = Hash::make($request->get('contrasena1'));
        }
        $usuarios->contrasena = $con;
        $usuarios->save();
        Session::flash('success', 'El usuario ha sido modificado correctamente.');
        return redirect()->back();
    }

    public function consulta_direcciones()
    {
        $this->is_loggin();
        $direcciones = DB::select("SELECT * FROM direcciones");
        return view('ADMINISTRADOR.consulta_direcciones')->with('direcciones', $direcciones);
    }

    public function alta_direcciones(Request $request)
    {
        $this->is_loggin();
        $direccion = new Direcciones;
        $text = $request->get('direccion');
        $direccion->direccion = $text;
        $arrayText = explode(" ", $text);
        $acronym = "";
        foreach ($arrayText as $word) {
            $arrayLetters = str_split($word, 1);
            $acronym = $acronym . $arrayLetters['0'];
        }
        $direccion->acronimo = $acronym;
        $direccion->activo = 1;
        $direccion->save();
        Session::flash('success', 'La dirección ha sido creada correctamente.');
        return redirect('consulta_direcciones');
    }

    public function modifica_direccion($id_direccion)
    {
        $this->is_loggin();
        $consulta = DB::select("SELECT * from direcciones where id_direccion = $id_direccion");
        return view('MODIFICACIONES.modifica_direcciones')->with('consulta', $consulta[0]);
    }

    public function actualiza_direccion(Request $request)
    {
        $this->is_loggin();
        $dir = Direcciones::find($request->id_direccion);
        $dir->id_direccion = $request->get('id_direccion');
        $dir->direccion = $request->get('direccion');
        $dir->save();
        Session::flash('success', 'La dirección se ha modificado correctamente.');
        return redirect('consulta_direcciones');
    }

    public function consulta_directivos()
    {
        $this->is_loggin();
        $directivos = DB::select("SELECT p.id_directivo, p.nombre, d.id_direccion, d.direccion FROM directivos AS p, direcciones AS d WHERE p.id_direccion = d.id_direccion");
        $direcciones = DB::select("SELECT * FROM direcciones");
        return view('ADMINISTRADOR.consulta_directivos')->with('directivos', $directivos)->with('direcciones', $direcciones);
    }

    public function modifica_directivos($id_directivo)
    {
        $this->is_loggin();
        $consulta = DB::select("SELECT p.id_directivo, p.nombre, d.id_direccion, d.direccion FROM directivos AS p, direcciones AS d WHERE p.id_direccion = d.id_direccion and id_directivo = $id_directivo");
        $direcciones = DB::select("SELECT * from direcciones");
        return view('MODIFICACIONES.modifica_directivos')->with('consulta', $consulta[0])->with('direcciones', $direcciones);
    }

    public function actualiza_directivos(Request $request)
    {
        $this->is_loggin();
        $dir = Directivos::find($request->id_directivo);
        $dir->id_directivo = $request->get('id_directivo');
        $dir->nombre = $request->get('nombre');
        $dir->id_direccion = $request->get('id_direccion');
        $dir->save();
        Session::flash('success', 'El directivo ha sido actualizado correctamente.');
        return redirect('consulta_directivos');
    }

    public function alta_directivos(Request $request)
    {

        $this->is_loggin();

        $directivos = new Directivos;
        $directivos->nombre = $request->get('nombre');
        $directivos->id_direccion = $request->get('id_direccion');
        $directivos->activo = 1;
        $directivos->save();
        Session::flash('success', 'El directivo ha sido creado correctamente.');
        return redirect('consulta_directivos');
    }

    public function consulta_remitentes()
    {
        $this->is_loggin();
        $remitentes = DB::select("SELECT * from remitentes");
        return view('ADMINISTRADOR.consulta_remitentes')->with('remitentes', $remitentes);
    }
    
    public function modifica_remitentes($id)
    {
        $remitentes = DB::select("SELECT * from remitentes where id_remitente = $id");
        return view('MODIFICACIONES.modifica_remitentes')->with('remitentes',$remitentes[0]);
    }

    public function actualiza_remitente(Request $request)
    {
        $this->is_loggin();
        $remitente = Remitentes::find($request->id_remitente);
        $remitente->id_remitente = $request->get('id_remitente');
        $remitente->remitente = $request->get('remitente');
        $remitente->activo = $request->get('activo');
        $remitente->save();
        Session::flash('success', 'El remitente se ha modificado correctamente.');
        return redirect('consulta_remitentes');
    }
    public function alta_remitente(Request $request){
        $this->is_loggin();
        $remitente = New Remitentes;
        $remitente->id_remitente = $request->get('id_remitente');
        $remitente->remitente = $request->get('remitente');
        $remitente->activo = 1;
        $remitente->save();
        Session::flash('success', 'El remitente ha sido de alta correctamente.');
        return redirect('consulta_remitentes');
    }

    public function consulta_documentos(){
        return view('ADMINISTRADOR.consulta_documentos');
    }
    public function modifica_documentos($id)
    {
        $documentos = DB::select("SELECT * from tipodocumentos where id_tdocumento = $id");
        return view('MODIFICACIONES.modifica_documentos')->with('documentos',$documentos[0]);
    }
    public function actualiza_documento(Request $request)
    {
        $this->is_loggin();
        $remitente = Tipodocumentos::find($request->id_tdocumento);
        $remitente->id_tdocumento = $request->get('id_tdocumento');
        $remitente->descripcion = $request->get('descripcion');
        $remitente->activo = $request->get('activo');
        $remitente->save();
        Session::flash('success', 'El documento se ha modificado correctamente.');
        return redirect('consulta_documentos');
    }
    public function alta_documentos(Request $request)
    {

        $this->is_loggin();

        $directivos = new Tipodocumentos;
        $directivos->descripcion = $request->get('descripcion');
        $directivos->activo = 1;
        $directivos->save();
        Session::flash('success', 'El documento ha sido creado correctamente.');
        return redirect('consulta_documentos');
    }
}

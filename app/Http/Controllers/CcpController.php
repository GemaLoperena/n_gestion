<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;


class CcpController extends Controller
{
    public function is_loggin()
    {
        $is_loggin = session('is_loggin');

        if ($is_loggin == "" and $is_loggin == null) {

            Session::flash('danger', 'Iniciar sesión');

            return redirect()->route('login')->send();
        }
    }

    public function busqueda_ccp()
    {
        $this->is_loggin();

        return view('BUSQUEDAS.busqueda_ccp');
    }

    public function ccp_lista_json()
    {
        $this->is_loggin();

        $direcciones = DB::SELECT("SELECT * FROM direcciones");
        session(['direcciones' => $direcciones]);

        $query = DB::table('turnados')
                         ->join('recepciones', 'turnados.id_recepcion', '=', 'recepciones.id_recepcion')
                         ->join('usuarios', 'recepciones.id_usuario', '=', 'usuarios.id_usuario')
                         ->join('remitentes', 'recepciones.id_remitente', '=', 'remitentes.id_remitente')
                         ->join('direcciones', 'direcciones.id_direccion', '=', 'usuarios.id_direccion')
                         ->join('status', 'recepciones.id_status', '=', 'status.id_status');
                         if(Session::get('id_rol')!=1){
                            $query->where('turnados.ccp', 'like', '%"id_direccion":"'.Session::get('id_direccion').'","respondido":0%');
                        }

        $jsonData =DataTables::of($query)
        // ->addColumn('Editar','<a href="{{route(\'editar\',$id_publicacion)}}" class="btn btn-warning">'.('Editar').'</a>')
        // ->addColumn('Eliminar','<a href="{{route(\'eliminar\',$id_publicacion)}}" class="btn btn-danger">'.('Eliminar').'</a>')
        // ->rawColumns(['Editar','Eliminar'])
        ->addColumn('turnado','OBJETOS.lista_turnados')
        ->addColumn('ccp','OBJETOS.lista_ccp')
        ->addColumn('btn','BOTONES.botones_turnos')
        ->rawColumns(['turnado','ccp','btn'])
        ->toJson();

        return $jsonData;
    }

}

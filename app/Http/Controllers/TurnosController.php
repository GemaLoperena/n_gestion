<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;
use App\Models\Recepciones;
use App\Models\ReporteRecepciones;
use App\Models\Direcciones;
use App\Models\Remitentes;
use App\Models\Turnados;
use Storage;
use Illuminate\Support\Str;


class TurnosController extends Controller
{

    public function is_loggin()
    {
        $is_loggin = session('is_loggin');

        if ($is_loggin == "" and $is_loggin == null) {

            Session::flash('danger', 'Iniciar sesión');

            return redirect()->route('login')->send();
        }
    }

    public function busqueda_turnos()
    {
        $this->is_loggin();

        return view('BUSQUEDAS.busqueda_turnos');
    }

    public function turnos_lista_json()
    {
        $this->is_loggin();

        $direcciones = DB::SELECT("SELECT * FROM direcciones");
        session(['direcciones' => $direcciones]);

        //$direcciones = session('direcciones');
        //dd($direcciones);

        // $clave = array_search(1, $direcciones->id_direccion); // $clav
        // dd($clave);


        $query = DB::table('recepciones')
            ->select('usuarios.id_usuario as id_usuario',
                     'usuarios.nom_usuario as nom_usuario',
                     'usuarios.activo as activo',
                     'recepciones.id_recepcion as id_recepcion',
                     'recepciones.turno as turno',
                     'recepciones.no_oficio as no_oficio',
                     'recepciones.fecha_oficio as fecha_oficio',
                     'recepciones.fecha_recepcion as fecha_recepcion',
                     'recepciones.hora_recepcion as hora_recepcion',
                     'recepciones.asunto as asunto',
                     'recepciones.observaciones as observaciones',
                     'recepciones.vigencia as vigencia',
                     'recepciones.asignado as asignado',
                     'recepciones.situacion as situacion',
                     'status.status as status',
                     'remitentes.remitente as remitente',
                     'turnados.turnado as turnado',
                     'turnados.ccp as ccp',
                     'direcciones.direccion as direccion')
            ->join('usuarios', 'recepciones.id_usuario', '=', 'usuarios.id_usuario')
            ->join('direcciones', 'direcciones.id_direccion', '=', 'usuarios.id_usuario')
            ->join('remitentes', 'recepciones.id_remitente', '=', 'remitentes.id_remitente')
            ->join('status', 'status.id_status', '=', 'recepciones.id_status')
            ->join('turnados', 'turnados.id_recepcion', '=', 'recepciones.id_recepcion');
            if(Session::get('id_rol')!=1){
                $query->where('turnados.turnado', 'like', '%"id_direccion":"'.Session::get('id_direccion').'","respondido":0%');
            }
            // ->where('recepciones.id_usuario','=',Session::get('id_direccion'));

        $jsonData =DataTables::of($query)
        // ->addColumn('Editar','<a href="{{route(\'editar\',$id_publicacion)}}" class="btn btn-warning">'.('Editar').'</a>')
        // ->addColumn('Eliminar','<a href="{{route(\'eliminar\',$id_publicacion)}}" class="btn btn-danger">'.('Eliminar').'</a>')
        // ->rawColumns(['Editar','Eliminar'])
        ->addColumn('ccp','OBJETOS.lista_ccp')
        ->addColumn('turnado','OBJETOS.lista_turnados')
        ->addColumn('btn', 'BOTONES.botones_repuesta_turnos')
        ->rawColumns(['ccp','turnado', 'btn'])
        ->toJson();

        return $jsonData;
    }

    public function turnar(Request $request)
    {

        $this->is_loggin();

        $turnado = new Turnados;

        $copia = $request->get('copia');
        $turnados = $request->get('direccion');
        $id_recepcion = $request->get('id_recepcion');

        $json_turno="";
        $json_copia="";

        $turnado_existente = NULL;
        $ccp_existente = NULL;

        $turno = Turnados::where('id_recepcion','=',$id_recepcion)->get();
        if(isset($turno[0]->turnado)){
            $turnado_existente = json_decode($turno[0]->turnado, true);
        }
        if($turnado_existente==NULL){
            $turnado_existente = array();
        }
        if(isset($turno[0]->ccp)){
            $ccp_existente = json_decode($turno[0]->ccp, true);
        }
        if($ccp_existente==NULL){
            $ccp_existente = array();
        }


        if($turnados!=NULL){
            $json_turno = $this->jsonTurnado($turnado_existente, $turnados);
        }
        if($copia!=NULL){
            $json_copia = $this->jsonTurnado($ccp_existente,$copia);
        }

        if($turno->count() == 0)
        {
            $turnado->id_recepcion = $id_recepcion;
            $turnado->turnado = $json_turno;
            $turnado->ccp = $json_copia;
            $turnado->save();
        }else{
            $id_turnado = $turno[0]->id_turnado;
            $turnado = Turnados::find($id_turnado);
            $turnado->turnado = $json_turno;
            $turnado->ccp = $json_copia;
            $turnado->save();
        }



        Session::flash('success', 'Turnado correctamente.');
        return redirect()->back();
    }

    public function turnar_recepcion($id)
    {
        $this->is_loggin();

        $direcciones = Direcciones::where('activo','=',1)->orderBy('direccion','asc')->get();
        $status = DB::SELECT("SELECT * FROM status");
        $remitentes = Remitentes::where('activo', '=', 1)->orderBy('remitente', 'desc')->get();

        $consulta = Recepciones::Join('usuarios','usuarios.id_usuario','=','recepciones.id_usuario')
                    ->Join('status','status.id_status','=','recepciones.id_status')
                    ->Join('remitentes','remitentes.id_remitente','=','recepciones.id_remitente')
                    ->LeftJoin('turnados','turnados.id_recepcion', '=','recepciones.id_recepcion')
                    ->where('recepciones.id_recepcion','=',$id)
                    ->get(['recepciones.*','usuarios.id_usuario','usuarios.nom_usuario','usuarios.app_usuario','usuarios.apm_usuario',
                          'status.id_status','status.status','turnados.turnado' ,'turnados.ccp', 'remitentes.remitente']);

        return view('ALTAS.turnado_recepciones')->with('consulta', $consulta[0])
            ->with('status', $status)
            ->with('direcciones', $direcciones)
            ->with('remitentes', $remitentes);

    }


    public function jsonTurnado($array,$elemento)
    {

        $valor = array();

        foreach ($elemento as $direccion)
        {
            $found_key = array_search($direccion, array_column($array, 'id_direccion'));

            if($found_key===false){
                $valor['id_direccion'] = $direccion;
                $valor['respondido'] = 0;
                array_push($array,$valor);
            }
        }

        foreach ($array as $item){
             $fk_ele = array_search($item["id_direccion"],$elemento);
            if($fk_ele===false){
                $found_key = array_search($item["id_direccion"], array_column($array, 'id_direccion'));
                unset($array[$found_key]);
            }
        }

        return json_encode(array_values($array));
    }


    public function respuesta_turnado($id)
    {
        $this->is_loggin();

        $status = DB::SELECT("SELECT * FROM status");
        $remitentes = Remitentes::where('activo', '=', 1)->orderBy('remitente', 'desc')->get();
        $usuarios = DB::SELECT("SELECT nom_usuario, id_usuario FROM usuarios");

        $consulta = Recepciones::Join('usuarios','usuarios.id_usuario','=','recepciones.id_usuario')
        ->Join('status','status.id_status','=','recepciones.id_status')
        ->LeftJoin('turnados','turnados.id_recepcion', '=','recepciones.id_recepcion')
        ->where('recepciones.id_recepcion','=',$id)
        ->get(['recepciones.*',
               'usuarios.id_usuario',
               'usuarios.nom_usuario',
               'usuarios.app_usuario',
               'usuarios.apm_usuario',
               'status.id_status',
               'status.status',
               'turnados.turnado']);

        $st = $consulta[0]->id_status;
        $prueba = 1;

        if($st == 2)
        {
            $prueba = 0;
        }

        $respuestas = DB::table('reporte_recepciones')
                           ->where('id_recepcion','=',$id)
                           ->get();


        return view('FUNCIONES.funcion_respuesta_turno')->with('consulta', $consulta)
                                                        ->with('status', $status)
                                                        ->with('respuestas', $respuestas)
                                                        ->with('usuarios', $usuarios)
                                                        ->with('remitentes', $remitentes)
                                                        ->with('prueba',$prueba);

    }

    public function guarda_respuesta_turnado(Request $request)
    {
        $this->is_loggin();

        $r_recepcion = new ReporteRecepciones;
        $r_recepcion->rr_fecha = date('Y-m-d');
        $r_recepcion->rr_hora = date('H:i:s');
        $r_recepcion->rr_descripcion = $request->get('rr_descripcion');

        $nombre_archivo = "Evidencia_respuesta".date("Hsi").date("dmY");
        $file = $request->file('archivo_acuse');
        if (isset($file)) {
            $extension = $file->getClientOriginalExtension();
            $path = $request->file('archivo_acuse')->storeAs('acuses_respuestas', $nombre_archivo . "." . $extension);
            $string = Str::of("$path")->ltrim('acuses_respuestas/');
            $r_recepcion->rr_archivo = $string;

        }else{
            $r_recepcion->rr_archivo = null;
        }
        $r_recepcion->id_recepcion = $request->get('id_recepcion');
        $r_recepcion->id_usuario =  Session::get('id_usuario');
        $r_recepcion->tipo_reporte = $request->get('respuesta_turnado');
        $r_recepcion->activo = 1;
        
        $r_recepcion->save();
        Session::flash('success', 'Se ha publicado la respuesta');
        return redirect()->back();

    }

    public function descarga_respuesta($archivo)
    {
        
        return Storage::disk('acuses_respuestas')->download("$archivo");
    }

    public function eliminar_respuesta($id){
        //$status = DB::SELECT("DELETE FROM reporte_recepciones WHERE id_reporte_recepciones=$id");

        $id_usuario = Session::get('id_usuario');

        //dd($id_usuario);

        $deleted = DB::table('reporte_recepciones')
        ->where('id_reporte_recepciones','=',$id)
        ->where('id_usuario','=',$id_usuario)
        ->delete();

        if($deleted==0){
            Session::flash('warning', 'Solo el usuario que cargó el archivo puede borrarlo');
        }

        return redirect()->back();
    }


}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tipodocumentos extends Model
{
    use HasFactory;

    protected $primaryKey = 'id_tdocumento';
    protected $fillable = ['id_tdocumento','descripcion','activo'];
}

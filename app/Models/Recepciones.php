<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Recepciones extends Model
{
    use SoftDeletes;
    protected $primaryKey = 'id_recepcion';
    protected $fillable = ['id_recepcion','turno','no_oficio','fecha_oficio','fecha_recepcion','criterio','hora_recepcion','asunto','remitente','id_usuario','anio','id_status','estado','vigencia','observaciones','prioritario','remitente','archivo'];
}

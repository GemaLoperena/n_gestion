<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Directivos extends Model
{
    use HasFactory;
    protected $primaryKey = 'id_directivo';
    protected $fillable = ['id_directivo','nombre','id_direccion'];

}

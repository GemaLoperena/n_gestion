<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Nomenclaturas extends Model
{
    use HasFactory;

    protected $primaryKey = 'id_nomenclatura';
    protected $fillable = ['id_nomenclatura', 'nomenclatura','descripcion','activo'];

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Turnados extends Model
{
    use HasFactory;

    protected $primaryKey = 'id_turnado';
    protected $fillable = ['id_turnado','c_copia','id_recepcion','id_direccion'];
}

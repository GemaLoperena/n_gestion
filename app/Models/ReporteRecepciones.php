<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReporteRecepciones extends Model
{
    use HasFactory;
    protected $primaryKey = 'id_reporte_recepciones';
    protected $fillable = ['id_reporte_recepciones',
                           'rr_fecha',
                           'rr_hora',
                           'rr_descripcion',
                           'rr_archivo',
                           'id_recepcion',
                           'id_usuario',
                           'activo'];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usuarios extends Model
{
    use HasFactory;
    protected $primaryKey = 'id_usuario';
    protected $fillable = ['id_usuario', 'nom_usuario', 'app_usuario', 'apm_usuario', 'usuario', 'contrasena', 'id_rol', 'id_direccion', 'profesion','activo'];
}

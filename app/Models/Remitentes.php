<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Remitentes extends Model
{
    use HasFactory;

    protected $primaryKey = 'id_remitente';
    protected $fillable = ['id_remitente', 'remitente','activo'];

}
